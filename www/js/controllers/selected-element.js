angular.module('starter.selectedelement', [])

  .controller('SelectedelEmentCtrl', function($rootScope, $scope, $state, ConnectivityMonitor, $stateParams,
    recipeWS, basicComponent, ionicToast, $ionicScrollDelegate,$timeout) {

    var recipeCount;
    var intervalListener;
    $scope.$on('$ionicView.enter', function(e) {
      recipeCount = 2;
      $scope.noMoreRecipe = false;
      $scope.border = {
        visible: false
      };
      $scope.titleObject = $scope.selectedSubRecipe;
      $scope.title_image = '<img class="title_logo" src=' + $scope.titleObject.catImage + '>';
      $scope.startAdTimer();

    $timeout( function(){
      $scope.cTop = 110+document.getElementById('myDivsel').offsetHeight;
      console.log(document.getElementById('myDivsel').offsetHeight);
    }, 10 );
    });

    //advertisement event listener
    $scope.$on("$ionicView.afterLeave", function() {
      //window.clearInterval(intervalListener);
    });

    $scope.$on("$ionicView.beforeLeave", function() {
      //window.clearInterval(intervalListener);
    });

    //start advertisement timer
    $rootScope.startAdTimer = function() {
      if($rootScope.recipeAdv.length > 0){
        //intervalListener = setTimeout(function() {
          $rootScope.adv = $rootScope.recipeAdv[Math.floor(Math.random() * $rootScope.recipeAdv.length)];
          $scope.showAd();
        //}, 30000);
      }
    };

    //Watch page scroll
    $scope.getScrollPosition = function() {
      var data = $ionicScrollDelegate.$getByHandle('selelement-content').getScrollPosition();
      console.log(data);
      if (data.top > 0) {
        $scope.border.visible = true;
      } else {
        $scope.border.visible = false;
        console.log('else', $scope.border.visible);
      }
      $scope.$apply();
    };

    //load more item on page scroll
    $scope.loadMoreSearchRecipe = function() {
      if (ConnectivityMonitor.isOnline()) {
        recipeWS.getRecipes($scope.titleObject.catId, recipeCount).then(function(result) {
          if (result.data.status) {
            recipeCount++;
            for (var i = 0; i < result.data.data.length; i++) {
              result.data.data[i].isSelected = false;
              $scope.recipeList.push(result.data.data[i]);
            }
            if ($scope.recipeList.length >= result.data.total_records) {
              $scope.noMoreRecipe = true;
            }
          } else {
            $scope.noMoreRecipe = true;
          }
          $scope.$broadcast('scroll.infiniteScrollComplete');
        }).catch(function(e) {
          ionicToast.show('Something went wrong. check network connection', 'bottom', false, 2000);
        });
      } else {
        ionicToast.show('Went offline. check network connection', 'bottom', false, 2000);
      }
    };


  });
