var App = angular.module('starter.invite', []);

App.controller('InviteCtrl', function($scope, $rootScope, $stateParams, $state, ConnectivityMonitor, ionicToast, getData, $timeout, $ionicScrollDelegate,
  basicComponent, $timeout, $ionicScrollDelegate, pickImage, ionicTimePicker, $cordovaSocialSharing, ionicDatePicker, $filter) {

  var check;
  var ipObjdate;
  $scope.$on("$ionicView.beforeEnter", function() {
    if($rootScope.language == 0){
      $scope.title_image = 'CREATE AN INVITATION';

      ipObjdate = {
        callback: function(val) { //Mandatory
          console.log('In English::', val);
          $scope.formObject.date = $filter('date')(new Date(val), "LLLL dd yyyy");
          console.log($scope.formObject.date, typeof($scope.formObject.date));
        }
      };

    }else{
      $scope.title_image = 'Einladen';
      $scope.checkLanguage();
    }

    $scope.border = {
      visible: false
    };
    $scope.SelectedImage = "img/NewInvite/Invite-preset-1-empty.jpg";
    $scope.imageno = 1;
    $scope.camlayout = false;

    if ($rootScope.language == 0) {
      $scope.formObject = {
        title: 'Title',
        date: $filter('date')(new Date(), "LLLL dd yyyy"),
        time: "from 9:20",
        location: 'Location',
        longText: 'Invitation Text'
      };
      $scope.placeObject = {
        title: 'Title',
        date: $filter('date')(new Date(), "LLLL dd yyyy"),
        time: "from 9:20",
        location: 'Location',
        longText: 'Invitation Text'
      };
    } else {


      var monthNames = ["Jänner", "Februar", "März", "April", "Mai", "Juni", "Juli", "August",
                "September", "Oktober", "November", "Dezember"]
      var d = new Date();
      var temps = new Date().getDate()+'.' + ' '+  monthNames[d.getMonth()] + ' ' + new Date().getFullYear();

      $scope.formObject = {
        title: 'Einladungstitel',
        date: temps,//$filter('date')(new Date(), "LLLL dd yyyy"),
        time: "ab 9:20",
        location: 'Grilladresse​ ​oder​ ​Location',
        longText: 'Hier​ ​steht​ ​Dein​ ​Einladungstext.'
      };
      $scope.placeObject = {
        title: 'Einladungstitel',
        date: temps,//$filter('date')(new Date(), "LLLL dd yyyy"),
        time: "ab 9:20",
        location: 'Grilladresse​ ​oder​ ​Location',
        longText: 'Hier​ ​steht​ ​Dein​ ​Einladungstext.'
      };
    }
    $scope.formObject.formtime = 9 + ":" + 20;

  check = {
    value1 : false,
    value2 : false,
    value3 : false,
    value4 : false,
    value5 : false,
  };

  console.log('check:::', check);
  });




  $scope.rmText = function(index){
      if(index == 1){
        if(!check.value1){
            $scope.formObject.title ='';
            check.value1 = true;
          };
        }else if(index == 4){
          if(!check.value4){
            $scope.formObject.location ='';
            check.value4 = true;
          };
        }else if(index == 5){
            if(!check.value5){
              $scope.formObject.longText ='';
              check.value5 = true;
            };
        }else if(index == 3){
            if(!check.value3){
              $scope.formObject.time ='';
              $scope.formObject.formtime = '';
              check.value3 = true;
            };
        }else if(index == 2){
            if(!check.value2){
              $scope.formObject.date ='';
              check.value2 = true;
            };
        }
  };

  //watch page scroll
  $scope.getScrollPosition = function() {
    var data = $ionicScrollDelegate.$getByHandle('invite-content').getScrollPosition();
    if (data.top > 0) {
      $scope.border.visible = true;
    } else {
      $scope.border.visible = false;
    }
    $scope.$apply();
  };

  //change image of invite screen
  $scope.SelectImage = function(url, index) {
    $scope.camlayout = false;
    //$scope.formObject = {};
    $scope.imageno = index;
    $scope.SelectedImage = url;
  };

  //get image from camera or gallery
  $scope.PickFromCam = function() {
   //var result = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAfQAAAEsCAYAAAA1u0HIAAAgAElEQVR4Xu2dB7QdtbmFt43pxlTTe+84pvdeQu81hJD2EtILCcnLI4VUUoCE9EJIiOmYTqg2BFOMKQYbMNhgWuimY4rxeWsfnfHRmTPSFM25njt3ay0WcM8U6ZNGW/r169egxh/RgJIIiIAIiIAIiEC/JjBIgt6v60+ZFwEREAEREIEmAQm6GoIIiIAIiIAI1ICABL0GlagiiIAIiIAIiIAEXW1ABERABERABGpAQIJeg0pUEURABERABERAgq42IAIiIAIiIAI1ICBBr0ElqggiIAIiIAIiIEFXGxABERABERCBGhCQoNegElUEERABERABEZCgqw2IgAiIgAiIQA0ISNBrUIkqggiIgAiIgAhI0NUGREAEREAERKAGBCToNahEFUEEREAEREAEJOhqAyIgAiIgAiJQAwIS9BpUooogAiIgAiIgAhJ0tQEREAEREAERqAEBCXoNKlFFEAEREAEREAEJutqACIiACIiACNSAgAS9BpWoIoiACIiACIiABF1tQAREQAREQARqQECCXoNKVBFEQAREQAREQIKuNiACIiACIiACNSAgQa9BJaoIIiACIiACIiBBVxsQAREQAREQgRoQkKDXoBJVBBEQAREQARGQoKsNiIAIiIAIiEANCEjQa1CJKoIIiIAIiIAISNDVBkRABERABESgBgQk6DWoRBVBBERABERABCToagMiIAIiIAIiUAMCEvQaVKKKIAIiIAIiIAISdLUBERABERABEagBAQl6DSpRRRABERABERABCbragAiIgAiIgAjUgIAEvQaVqCKIgAiIgAiIgARdbUAEREAEREAEakBAgl6DSlQRREAEREAERECCrjYgAiIgAiIgAjUgIEGvQSWqCCIgAiIgAiIgQVcbEAEREAEREIEaEJCg16ASVQQREAEREAERkKCrDYiACIiACIhADQhI0GtQiSqCCIiACIiACEjQ1QZEQAREQAREoAYEJOg1qEQVQQREQAREQAQk6GoDIiACIiACIlADAhL0GlSiiiACIiACIiACEnS1AREQAREQARGoAQEJeg0qUUUQAREQAREQAQm62oAIiIAIiIAI1ICABL0GlagiiIAIiIAIiIAEXW1ABERABERABGpAQIJeg0pUEURABERABERAgq42IAIiIAIiIAI1ICBBr0ElqggiIAIiIAIiIEFXGxABERABERCBGhCQoNegElUEERABERABEZCgqw2IgAiIgAiIQA0ISNBrUIkqggiIgAiIgAhI0NUGREAEREAERKAGBCToNahEFUEEREAEREAEJOhqAyIgAiIgAiJQAwIS9BpUooogAiIgAiIgAhJ0tQEREAEREAERqAEBCXoNKlFFEAEREAEREAEJutqACIiACIiACNSAgAS9BpWoIoiACIiACIiABF1tQAREQAREQARqQECCXoNKVBFEQAREQAREQIKuNiACIiACIiACNSAgQa9BJaoIIiACIiACIiBBVxsQAREQAREQgRoQkKDXoBJVBBEQAREQARGQoKsNiIAIiIAIiEANCEjQa1CJKoIIiIAIiIAISNDVBkRABERABESgBgQk6DWoRBVBBERABERABCToagMiIAIiIAIiUAMCEvQaVKKKIAIiIAIiIAISdLUBERABERABEagBAQl6DSpRRRABERABERABCbragAiIgAiIgAjUgIAEvQaVqCKIgAiIgAiIgARdbUAEREAEREAEakBAgl6DSlQRREAEREAERECCrjYgAiIgAiIgAjUgIEGvQSWqCCIgAiIgAiIgQVcbEAEREAEREIEaEJCg16ASVQQREAEREAER6DtBX3x9YK1jgaW3ABZaDphvcWDwvJ01MOtN4J2XgdceBV64E3jgNNWQCIiACIiACIhABgK9FfR5FwG2OQNYaS8j4nnT7FnA69OA6aOB8d/Ke3e9r196K2DPSwEMAtAwZR08HzB1FDDu8/UuO0s3fHNgx78Bg4eUV1Y+a/b7wMwXgBcnAHd/H3j/jfKe39+ftO9N3d/xe68D1x8CvPV0fy+d8i8C/Z5AbwSdQr77RcAKuwKD5ikH0rszgPt+Bkw8tZzn9fenrH44sOuobr6PXQjccHh/L116/pvlPxcYNDj92sJXNICXJwITTgaeuKLwU2pxI7/pY54C5lu0szgcAF08AnjlwVoUU4UQgf5MoHxBX/tjwLa/AeYd2hsu/x0DXLlLb57dn5668j7AnpcNXEF3lb9Xdfjfm4Ard+3V06v/XAr60dOB+ZfozOusmUbQX3uk+mVQDkWg5gTKFfRtzwQ2OKFlBu4hudenAhePHNjmUAl68oCmh80O774C3HgU8PS1vXxLNZ8tQS9WL1waoyUp7i/0zovA5TsM7D6sGFHd5SFQnqDvdBbA2XmexDXy2e8C/DfXf4csmP3uN6YDF208cD8ICXrfCzpb5wfvAFfsDLxwR/a2WocrJejFatG1NEbLxujNtFRRjKruchAoR9BpYt8ggyPWB+8CM+4Hnr4emH4p8OJdndmiJ/yaxwCrHgDwv5sOX570/G3AZdsOzMqVoPsFvTlIzOAw17yOfh4pbc1uZbQQnbfWwGp3TkF/y1jLZHJPbg+u71RLFQPr++mj0oYL+vonmDVzn3MSO82p/wLG5pjBLzUS2OHPAP/tS/f9BBj/7T7CVaHXSNDdgs7B4nUH5aus5XcxWypX2M38mwLmTA1gwneBe07J947+fLVm6MVqT4JejJvuKkQgTNAXXBo44lFgvmHul7/5FHDNXsVNSyNPBjY92e0tP2uAzhAk6G5BL8PTf6ezgbWPdc/c2a5HrVzoo+u3N9FqNmRhoPFBuwgcrNPqpqQZutrAXCcQJugfvsbsMXclfugXbRJeyI2/Bmx5qtsKQBP+1XuEv6c/PUGC3ltBZ1vw+YU0ZgPXHajtbP3pm5kbedUMfW5QH7DvLC7ow1YHDn+423szQvnqQ8AFXAcvKe12IbD6ockPG4jrURL03gs6W9sRU4BF105udw/+Drj1cyU1cD2mlgQk6LWs1qoWqrig734xsNrBDoHtgRmca3hHPAIstGzCOxvAuC8Ck8+sKufy8yVB7xtB3/xHwIccPhplmPbLbxl6YpUIOAW9B31klcqtvMwVAsUEPW3tfPJvexN+dKtfADS/J6Vp5wM3Hpkf4vI7Aet8wjjfsVzcOmdHt5v1NvDGEwAD2tz9vd5tk6Nz4dKbA0NXBgbPb8rBKFz0qOa7GdLVTr0S9NUOAfjsxdcD5lus7SlOK8jM54GX7wMeOGPuh/r0BZYpU2hX+jCw1xXJPhxlLCmx3Y04CVh8A2DIQmZb3NvPAWOOzc54nY8Dqx5o6myBpVpWs0FmiYpLAwxf+9pUE0L5/l/m/0Z6fQd3yCy3IzBsNWDeYcCg1o6D994A3nwCeO7W3uab/Gj9W2xdYP7WGRNRH0B/gXdeAl59BHj8IuChP+WjoW1r+Xjp6iACxQR9s1OAkd9JfvF7rwEXbpi9M8qT/aaZ/yGzZz2e8pr4N/8hsM7xwELLZ88BHYCeuR644YhyhJ2e1XT4W2br5DLZOSNXhh9lR89UpqDT+sEdBSvuYTq01NQwB+g8/Bdg4s9Tr+7JBX0l6C7vbhaKDM5PMMcvuy2wzw3APAt0Fv2le4BLNm3/be/rgBV363a8yxpOded/mi2eXo/8GH3GXp92LvCfz+SrFr7j4LvNoNdO778JXLpV/u+dA5mtfwUM3yJb/Al+e9zmyl0tZYXh3e53wBpHZmzzrUIzzv+Us4DxJyXziyYdkeNgauhrnsPQGsAwvDV9jhQXP1/b1NVzCBQT9IPuAoZvloyxyJahPBXCdy+xEdCY1b6LM9qX7jYdS1pih7TfzWY0XjRxFjXpN8Cd3yj6BDProyjl2f/MtzFa2U3HmNlXGaFfaVLe+Kvd4pO1ZG8/C9z+VWDaeVnvKOe6vhL0hVcEDn8wWTRdgu6alUXX85lsx4nLRwDSfEJWP8wMwOJx1fOQZaQyrv/TmpElNQc2T3SLX5EAKXtcAqxyQPE4/Iyvz/MKiu5958E+zAProWjiLodbPtUdNbAZLbOgX0URlkXzr/tqSSC/oLs+bOKpuufvinsCu1+Yb0bjq3aaAi/fPn/DOOQ+YMkA73/O4HhgCC0l8eApeczNe13ZGlTkL0LnHQ1g0pnAbV8MfVD2+/tK0F3izJzGZ9xR7l15oxXpqj3cA4Tofp+g07JEE33qzC8Lyoaxstzy6fSLy9iHHs3yFy0hKA8H1WOPzz+Q5GBo538UH8DapNjfjT0OePSc9l9DBV1x8dPboq5wEsgv6L41Rc7Wzslhwu7LimFM5f3GlPMh2/l+43FjJst6zOYBtxkTe69SVkHf90aAJv8yU698J5Ly2FeCzvVVzoaTAie5WLvyNuMBY14etqafuiu2AsW86aCXI6pdlvp9+jrg6j39V4YKOu+npSNkVhzPIQe2PLo1qwmeYr7LqGwRBLNw4zVcChu9RdtaECroCgeblbyuSyCQX9B9Xr/P3gxcsVP1QNPMzi12vvVhrtG9MtkEyaDzFzsgziRo3l9guL9Mrpla/K6s8e75fprWOQujWTbPGmkWQd/zcmCV/fxl4lorHfLYYVGE2BHT38B7XGkDuOPE3jowpc2C+XsWBllb6YG3AxwMJiVXlMLQk+CS1tB5TsKOf/HPzDmo5KCBR5my/Sw43CwtLb5h+umHD/8VuOWTbiqhgn7kNIA+ML7E9Wm2OYaI5nICHUTp5OdLHPxQUNOOb+W7D52Usl7fAF5/DHj1YeMIx5Pl6KyYlu+nrgGu2dvkcqufG8fd2VbwHVcIYq6zN7iG3kpcQ6ePhdbQs36dui5GIL+gc7ayruPDZ3jXmz5SPcj73wIs6zKNN4Cp5wE3He3ONzvTkf/rmVllCAXqs2xEb37uP8DNn+xeG6QHPGdmC6+QzjZNzNjZsNNxzfIoBA+cDtzzg+53sVOnIxFnOvO0PPHjV/XVOmBfzNB9AY0oujwtK+mQlsS8tZyf2InHzeVcz6YgcRBFEaZ3N2eekdWH3I+c2u2QFrHnTgw6J3IXhitt+j1goy+7191ZHvqgcHCalEIEPW0ASd+CO79pvPDjiVakD30LWIFH1zosEy5fBvtZh9wLLDnCzYc7ScZ8NFlMufNjm9Pd1oXcbSGDn0T6l64rRKCLQH5BP2g8QKeSrtQwTjYP/r5amH1roFwDG8c8/yFbnn1m6rTlBp8jIa0Dd3wNmPRrfz6ajnT7+q/xCXqaMLw4ARidVLexV9Licdhk9+yp146RzE6vBX2LHwMbn+g2z3ImzNP+kpIrb/EDYzh4mvgz4L6f+evUZ8blMyjEWRzE0szePgtbUUGnx/++Yx0cG2b9mUKaltb/bOvMCB6kE08N4M6TgImnJj/Ft2yCBnDvj4G7HLt27Cf6rDWuyYwvsIzM62m1rt9zEihX0MfQQeSfObPQ48sPGAcss03ySx44zXho50lHPQYsslpyp+I6sMM3O6dzz3UHAzTbZUk8W5lbbVzJJ+i+ffzPjQMu3y5LDsw1NEPT8hE/55m/0XQ4atXsfgXZ39q+0ifoj/zdOEzlTYxVvsEXzJ5ulwd685kpg9csJvese9h9g7AiTqg+07Nvll5U0J3WsYaJZ3D7V7LXkm+7LLe00fSelHyD6TSLlv0839IdLSwXbtD9dkWKy16/ujKYQHmCXsVDUnz71l0fYBpSijNNiEnrYq5Zm9PkWHDN+bBJZm0vKfk6qKOfBIau1H0XTb7nrpFfgJ3+FA2g14M7n2hyDZYWkzxp/sVa5ugMDme+2TnfmSboedreJt8Atvxpsrm56BKXb2DXnDG3Yh3Y/IoIui889At3ZttmGq9Dl5WMyxW0LsUtFb6BZ5F277KWuOJvSNDzfIW6NpBAiYI+E6jalovt/wisl7QlJ3B5wDXrT9puVCQwSVqlFvG89t1T9Aha3+yxqNiklT36PU00sz4n73WsY1oyXGvNaYLOWTUdR+kvkSW5ZpchA2jW21HTkp09XevRRQR9p78Dax/XXUoyuGzbZP+DNCZOE34DGPtxgNYZO/l8frjU4QoQ48oHnUMZgpqOonZyraNL0NNqVL+XSKDegu5yhKE37Xlr5p+RRuBH/h+wWYLTGE2x8U6lV0LKQ0MWWq67Kbhm6C4rAWey569TnMV+Y03YznhKm8WGNuK5IehcA7/5+M59x0nl8OUtz04QX4jl528zolg0uU5KdA0Uigi6y7M9D4N4+ZpnOjjaftIg0tUHhAyIEsvlmCRI0Iu2UN1XgEB9Bd03M37yKuDfKc5lPpg+Uz6dAm89oX23a4ZAZyY6VRXdorLDn4B1P5Vd0Lltb7F1uq8PFQaXCTK0fGmNua8FPYrQl8XXwZm3nJYh33N8TmBp7Pi7y5SfFCyF1+cVdA5GjnrcxKePpyK+K/YzXI658e2jvj4gZMBJz/tha3SWirsXpvytu6wS9CytUdeURKBEQa/Y6UGMCsdIaF1r3SWczOYzNcdnyK7OJ8tWG18lb3IisCW9o2NrvkkzdN9Mr6gDWZQ3mkAZSje+FavX29f6StC5JeyZG4BrD8j+yTm93FO2hsXf4FrrLoOt01HT8X3kFXQ6bu5yTsK++YY5CyFryNkk6s293l/v/iX+TTn7AAC9XhKKcidBz/7d6MpgAvkF3eW12hzZf6w6Xu69/pBcQm2f+uYzD4ZaCVwdcpKg+7bucZmASxCDhhRrTNyPPu/Q7ntDTJpZcuIV9AbwzowCEcEawAfvGS/9V6cAjKDGM8/zJmfbyznodVk/QgeDvhm3y4M/r6D7ttpxQML2USTiHWfCCyyZHGAnzqUsS0ne+rev73U/FJI33Vs7AvkFfdvfADzusCuVMPMtE69rBpt28EXWPLgE3V4f9MW9z7NdJilPLpFOem5fzWY78pnTvJyVe9rMh7+7PLXzvqPo9WV14i6/h14L+n0/BcbHzoAvU9CLck27Lz6IZEAm9ldd0Q37sK8qqy2klV2/iwCHyI0/ckieI/lG3n1hxorOj46OJ4yyzv3c477Ydu5yebiXNXN0CTpju5/bCnHpW8MLFXSXObFKgs76mHxmjsaV49JeB5bJkZWuS8vqxF0OXT0VdABJgYH6haDHdtq4/EzKWLLI2j7KagtZ36frBjSB/ILu60izxjQPQe7aChP3MO+luZL5dwn69MuA6w40JeyloDe374zpDuwiQS83lnuRtlpWJ+5qY6GDQW/bdERd6y+Cbkdfc/UBZQ3qs7SNstpClnfpmgFPIL+g+xysXMEdysTsCqoSN6U7A0A4AlDkzaPrCFS7s+2loFfd5M4tXtcfnP0krLz8B8IM3SXoIR7aEWfnd+xYKukPgh4PFCNBz/tV6fp+TiC/oLPA3Ae66NoJRc9wSEkIMFdQBz7zzaeAUSu3n95LD+Gsa+M+QQ9dnnAJWq4Zeh+uJYbUe9K9A0HQXQGMyjC5Ox0lS/Jyd4rpTKCvYpi78tCXDryaoZf95et5HgLFBH23C4HVD01+bBmzB1eG8xzd6vICL8Pc5rNS8KSyKD61T/hD9383D6s4s9vhJ6+XOw+Eue1L/e8jGQiC7nJAffUh4IL1w+rMdRyrKz583hm6a/26SPz5oiWVl3tRcrqvnxIoJujOPaY8t2K2WUN+4orykbiCo/BN8fClvfyYnUeQJkSKczk2xS0KeWnlCSzTtGxMSQ7yUcZ6bN68l3H9gBD0M4ENPtdNq4xBqcuDvqxIcT4P8746ldHXB7gOUsrSNsmOzrkM9xolOuny6Oj4cbqaoWchqmtKIlBM0Ply56ljMHGuL9m0pCy2HuMLoZp0SlRzFv1I8vnPjPZ1zd7F8+cMm5lgTtz9YmC1g7vfFTrwcS17uAT66OnA0FW68xE622PHxrPa43uK2bHxjO5epYEg6L7gLOO/lX7sqo+966CesmK5+6Ip2rEairQPDhaa56PHEoMk2RMJn4XspXuBS0bmf7szAp7jQCIJen7GuqMwgeKCPvJkYLPvu19sm54LZ691Y/P87UnJh0nwkqevB67eo/stPLObR2LGE8N4jlqlWPzyvIetOOO+o7g3tu84Vpegu06pyntYSJyla3AzYxJw0UahNe++fyAIui98qu+40DTqRdpPXpO7L6gSzw84Z/m0XLp/dw1GppwF3PzxzvtczqtFt665vmeXZUOCXryedWduAsUF3ffBNrNR8GjQpCIcOhFYYuPkwvnOcN7pLIBrhV0pwHnPJ9CTzgBu+3Ln23yOfEXjnbtElG92CbrvGM7po82Z7HmTz5Qfj2mf99lp1w8EQScDlyC5TvdK48bfXe3HZzXKK+jN91wNcPCQ9P0VjVHgMuUz79cfCrAt28kXN6NIG3XVh8vSJUHP0iJ1TUkEigs6M+ATiaamzwbu+o5Z3y6aDrwd4JnGruRbA/aehfwScPGH8h2O4hvE+Eb8rnC5LFNeMfWGcfUIus+ywKA8PLnLdyRoEv9ezfqztJWBIug+S1iRM8Wdx48C8HnPFxF0nyXADsCUpb6ja1yzc9fZ5j4rR95ttj7fIdeuFQl6ntrVtYEEwgTdN4OwM/bUv4EbDs9n4h6+ObDrKGDYmu4iuj5i+w7X1h9ek3cdzTcz5gEeV+2enFdfR0BLxoSTgXt+mF6VHKDse1P3Wcz2nb4Bju9s6DemA+eulp6H6AqXbwB/7+VOh+j9A0XQfWeXk8Uj/wDGJpw57qpJn+/L3d8D7nYsoxUR9Gb/cC+w5Ijk3DBW/tV7ZmtzfP+BdyQvofEJSeb26Mm+7zZrW20u+00GFliqO7++ZStfXP/z1803ochGSlcNYALhgk7nl0Mn+UWGgD94F+CMgg4xvgMvGIN9zaOBJTfxH97gMrHFKzMtjvmM+4Fr9kn/sPa6AljZceRqFvOnr2OjqE/+HTAuKUZ+q0A0NW79S2CeBdolZPCW+GlyPkFnp8SdAvMvntzk6Xl/zV7AKw+6Pwk+Y58bgSU2TL4mdE0+68c4UASdPHxmY/7++CXA9Yf4yVEQD7oTWGy95OvS1rWLCrp3MAvguVuBy7f3533t44CtfmkOZUlKaXn3OejxeWzvHFi4jjJmn7TZDzq/PTsfvi2oPovao/8Exnw0a4vXdSKQSiBc0PkK155W1+spRDzRiv/QVE1RWmC4Gf12HXea9JAcs1re7huh83fm58XxAL3fn7oWoMMR07qfAFY9GFh+55RZ8QXmSEhfcoVqte/hmjoPd+E/z40DuBVm2e0M36U+5BDQDzpPnkrbhpbmzMjBxcv3myNDn7nRdLhDVwJ4BjRjDzA/8aNS7ZyFBsxJbbKtCwaSoLPILlNzxIvm42fHmjb86L+MNYwOoavsB6x6IDB8i4RDSqKbMxykU1TQs3x/zcH+HabNcdBPJ1funlhlf9PmFt/A0yoy5J13+6xTzaez3U80391rU4BZ75hBKwfxHBC4UtpyVdpgIuoHedohLY5X7prPkpn1e9F1A4JAOYLeFPXjgO1+CwxZuLfgiqzLszOiY90iOUzKWUuRx2y/+Q+Tt3hlfVeW69IEnc/wmcuzvMN1TVbzZcg7onsHmqBzuWW/Me5ZYgjTLGb7EEFn3pzRJUMynvNkPZ8vS9FsPHAacPtX/XenDcaiu2npu3iE30JWNJ+6b0AQKE/QiYvmWK5Z+9a9Q7ByFvKf/wGmnZf/KU1Rvx9YZNX897rueO0R4Px18j2Poj7iW57ZUsbHPfRHM4NZaLnOG7IIOu9wHnKT8f3xy+jlO3rLvptdDDRBJ2+esLfnpeWKetLJaklNIFTQ+cyD7gKGb1awgSXclmWpIX7bwROApUqKkZF1P33Wb62so53LI6wn9TMC5Qp6VPidzgbWOsZvms0L6vnbzToXTYlFEzul/cYac15oKuJhHL0zaT08T344oLnl08DRT3Svh2cVdL6PEe84wLDX5fPkI7o2j3NTkecn3VNlQXetmxbd+2yXnzN1ivqCywSSbLQc6pK2dSY8ugxBbw4kzwbWPtbvH5NasgZw/y+BO05MvTLxgr2vBVZMiFuR9Wmz3gbuOQXgufFZUlocjegZZUQAzJIfXVNbAr0RdOJiB7Db+cByO6U7zLnwcm375fuAe3/cvb80pEroYLPuJ4H5huV/yswXAO5fpUdwSGo6l90ALJEj+ArX2+77GTDxVMP3yKnGKmKnrLOG6B4+hz4Gy26Tv5N9/THg3h8BU/4WQqLYvb4tUQ//Fbjlk8WeW8ZdzbPqr+g+2va914DRWwC07ISmPUab9XGfP4PrHa9OARhpLr5n25ensgSd71hxd2C73wPD1shJoQG8OAEY+7FwszTPQhhxEjDUOtApLTdc63/icrNjJ2/KYr3UDD0vVV0fI9A7QbdftMHngVUPABZb1zi+uWaE/GDeexV4bapx8OEe9l6mEd8EVjvUdCzzL5YsaBxUvPOCcZiZei5Az9QyEzuW9T5t2CRx4TIDnXQo1JyV9CrR6W2Tr5s9/y4veNbPzOeAGZMBOr9NHdWr3Oi5WQlsczqwwu7GP2TIgsl30XHrrWeMs+cDZ3THG8/yrjIFPXof2/46xxvnPZfvDWetbz5t8j75t8Xy7ivfaocAa33E7KqZfwlgnvnN1XRSa8wC3n8L4J75Z64HxjPEcWBiX7jM1sZjf/B8AGf7tDpycMzlj8ghN/A1un1gEugbQY+z5Qe88EomzjpnLUzcesItZHMzrX5Y54zn9Wl9+4FRVKMZN7nQwzxkiSGEZcSCnvdM3Jozt/ISUo6BdC+/K3qER7N2OpC++nA531UvBN2uG+Y9igbZbHOzjbe7kgiIQGYCc0fQM2dPF4qACFSCQK8FvRKFVCZEoH8TkKD37/pT7kWgbwhI0PuGs94iAgEEJOgB8HSrCAwYAq4og3LkGjBNQAWtPgEJevXrSDkUgblPwLWrQII+9+tGORCBFgEJupqCCIhAOgHX3u0y9tanv0iUl8oAACAASURBVF1XiIAIZCAgQc8ASZeIwIAiwPXyfa4DFlzW7ERxbekklCwnHg4oeCqsCMw9AhL0ucdebxaBahKgoB/zlBHztPTkVcC/HacQpt2r30VABEolIEEvFaceJgI1IODyaI8XjfvcKeY84U1JBERgrhOQoM/1KlAGRKBiBDIJOuOpnwbc8bWKZV7ZEYGBS0CCPnDrXiUXgWQCaSZ3hiOedDow4bsiKAIiUCECEvQKVYayIgKVIUCvdh5cMniIydJ7bwBvTgd4st6Df6hMNpURERCBNgEJulqDCIiACIiACNSAgAS9BpWoIoiACIiACIiABF1tQAREQAREQARqQECCXoNKVBFEQAREQAREQIKuNiACIiACIiACNSAgQa9BJaoIIiACIiACIiBBVxsQAREQAREQgRoQkKDXoBJVBBEQAREQARGQoKsNiIAIiIAIiEANCEjQa1CJKoIIiIAIiIAISNDVBkRABERABESgBgQk6DWoRBVBBERABERABCToagMiIAIiIAIiUAMCEvQaVKKKIAIiIAIiIAISdLUBERABERABEagBAQl6DSpRRRABERABERABCbragAiIgAiIgAjUgEBvBH2LnwCr7AcsuAzQ+MBg4r9ffxyYOgp48HfJ6NY+DlhoeQCzgcm/A95/w1y3yYnAoCHAOy8BD/+5OPYlNgZW3oeZAZ65EXjxruLPiu7c8AvAkKGdeVt8fVN+DAZm3A88eVX4eyIGZT1v0+8CQxYG3n4WeOC08PxleYKrfrPcq2tEYKmRwOqHA0MWAF4Yb/qSrClqex/MBB44Petd/fe6XvRB/ZfGgMl5uYLOj2brXwHzL+EH+PpjwA2HAS/d075u3kWAY54C5lvUCO64LwKTzzQCvOflwKDBwOz3gQs3BF57pFgF7fAnYN1PmXsfu8jkISS58ma/5/WpwHlrhbzFdGK7nmsYvDEdOHe1sOeVyTRrTlz1m/V+XScCRb+rgdj2irJSK+vXBMoT9K1+CWz0ZSM6WdLsWcCYY4Fp55mr+dEdPb01GGgAt34OePD3LTEbBQyaB5g1Exi9GfDKg1ne0H3NtmcCG3yuJegXAjccXuw50V1NYbyslbe3gItHmsGG/Z7XHgXOX7u895T9vFlWvsNy6b/bVb+9fGdZz17pw2ZQOXiIaXsXblDWk/WcOAEf66LfVbPtPQHMv3jnZKHO9IuyqjOTAVC2cgR9sx8AI/+vExeF56E/Ag/9CVh0LWDVA4C1jweGrtS+7t1XgIs2Bt562i3oK+4J7HmpMbm/94oRTV5fJPVU0GcCF48wgr7VL4CNv2pySCvEJZsVyW37HnvgULqgW/kOy2V9Bb1s/r3k3N+f7WNdVKQo6Ec83FoCbAA3Hw88ek5/J+XPf9l9UL1p1aZ04YK+4NLAEY+0TOXk0gAe/gtwy6eTIe1+MbDawe3fpp0P3HikW9DLRN1Xgl5mnvmssgWlw7IQaPXIWtb+PEO3eb36EHDB+llLrevyEvCxLiroefOg60WgnxIIF/RtfwNs8Pl28R/+K3DLJ9042LEfORXgQIDpjceBc1f3Czpn6TSXvT4t2ZFt+V2ATb5uRuBMs98zM2Oa7e2UJuh0mlt+J3MHzftpDnhxYYxm6Lx/9cOMKZ4OcZFzX/R8+9mb/wgYvimAQQDN38/+p9tJLYugsw6ayx0N4PHRfitGfKng/HWBNY8CyHmeBYDB8wAzJgO3f6Wdd1eNbvFjYPgWQGO24f7yROCu/+2+Oqug7/AXYKkRhgcTnfZo6Xniis5n0l8j8reY8vfOfNIhaIXd3PVo/07nqhfucLdX1iOdsTb5huH75lPAXd825Y07ZfE6WqqGrtxqh7OAVx82PIpalfj+tT7Sbtvk8vxtpm58aeTJwDJbm++KbYIOpc/dCtz/y867VjsEWHgF87fnb3c7ivp4896s7cCX5zTW9vcbDaz4/Sy9pWm3s94GXpoAjP9291vYR7DPee814Klrun8f8U1gxT2AwfMbpzt+o9NHA/f/ys+5jDqIP2Pr00yfwP5jnvmBt58HHvpD9zeQlrOkPojtgXU5iO3oDlPfdJBlWxk8H0Cr6SN/b7+LfdaGX2xbVt9+Drj3R34/pqxtL55/fj/sx/hdsy95ZTJwzw/NVfyNyfa7su9n/a7/WWCBJY2v1czngccuzM8sjWnFfw8T9KYpawqw0HKmmGwMo1ZJF4GdzgLW/pjpaN58ur0mmbSG7lqnjsDudQWw8r7JmD94B5h4KjDhu+Z3n6Cz8e/8D9MxMLHDvmIn4Ln/uKvQJehJHQ+fctB4YPjmptyP/ANY/VDjaR5PHORcvkNbBHyCzjo4/EFg4RXbT7nvZ8D4k7Llm74MM5/rvD+6k/zu/j5w30+7n8WPh7sZmqIaSx+8C0w7Fxh7fPuHNEFnJ/Chb7X5x5/JeiATpmGrA4c/DAye1/z/hJOBe05p37H7RQCFKqrH6w7s/LDt31+6F7ik1VnE38n1XLYvdqzxFHfQ3PU8YA36ZLQGIvb1ZDz1nE4e7toxv5DXAbcC7FCTEr+1W09o+6BE19DUyroZslDyfe+9burzvp/EBtEwOzIu2qT7vmW3BfYda3wImO78pvmumPK2A1e5s7CmsEQ+MBwgsV6ivsd+Lst49Z7tgVq87Y39uBEtJg5idz7bGjDFMsgltCt3zTcgy1MH9us2O8Xs6KGIJ6WnrzPlypJcfZD9dw5sFlsPWGTV2BNpZf0rwO+Y9dvlF9UA7j8NuONrnfcVLTefssclwKoHdn8/784Anr2l9ZvlW2W/ef9bgGW3T6ZCB+xr9yvud5WFdYWuCRN0jnjZsTadTWBmo/92iGtaoV0dvs88fODtwNJb+Z9MYb7+UDPadgk6xXyXf7UFgh0wO8uiM3SXaXCOoKfBAPDszWZAweQS9CQxby53tDz5Xa+Jnsffk8Sq474GMOF7wD0/aP+VsyKKb5J42fdOvxS47iDzF5+gx608rnxzdkyHNFo8OPgbuoq5kh3TNXu37+Igc1HLEZHOlazPKNFCNGwN83/cQhm35ETX2bsB4nmioNMiQyc5X4di3/ffMcCVu2SofACHTQIWT3G+Yx4oNtGgc40jgV3OSa9TfhPRIOfD1wAr7WXy5HI6nTMA5zWWE2WRduBtk63dLC7W65/QEvSGaXvcCutqvxT8f7X8deJtL9pB01wufBSYb5i/TmglOn+d9IkKn1KkDngfhZP9RppTsd0v+HLt6oPsv2driclXsQ2xr4+sHUXLzacfcJuxEKSmmKCzXg+dCCySsuuHbZaTAdfsPvW9/eeCMEG3hcbealak/FkFPTJrNzsTy7TGWe3EX5hR+UZfMSbk6GNvOqZtmizoSWKe1WkmywzddmLrEvQG8NS1wOOXGLMnzX6RhYCNkKZwdkxJgs7O6JD7rBlKw+wKcImTXSf28zh44cyLpjTuR3/mBmM2phNjJNgcJY9a1XRoHEBRwKLZMWfxdHx85Gxj5madzJm1Wx+gq37jz6PZlIMHWhm4XY9LKU2rRivRjMbdCbtdaCwcTBT6US0zNy0V9OkYsqCxhLAMNCuO3sJc25zdP2TMi1msMJwxDVsTWPeTprNl585BwPtvGV60LGz2/Xb+mJdxnzMWAd674ZfaJu2s30hzm2JrZwfvoZ/JHSeatrDd74B1Pt6exb1wJ3Bpa1B7wDhgmW1MXlgvk35tZtOc5TMv9jcRcex4F8zMPW6yPuqxdqcZCUrRduDrG9JYJ4nRixOAKX8F3pkBbPkza7bZAMYcBzz6T/dg0t7axXZHXuO/ZXjR+rTyh9vfAH+77UvpPVuROuBTD7kXWJJLTa32fPf3gCl/M0t3/B6X2Mj8Zg/GfLnJI+hciuE3zH6hy8rU6qM4IVrlgE4mURtqinKBtsf7Nv8h8CFriY5tnHXG7bnr/U/b1G4K3979xP/d90aApvYosZ/nkhL7XC4BRctu/H2A+L6UK+jRVrP0Zt99RV5Bt2doM18AzluzcwRtN2ium7FDt0129HLllrk9Lm2bEjnjGfPRbjOmqzxBgs6ZL03FrTUivmPjrwFb/bw1+7BmUXFHodFbGtFaaNlWzhomEM84y5fBVwcdAzEkb8Xa/o/AepZjY9TR2x9R0siXgnrYA8B8i5kccIBw1e7uTpXbwZpBeOj7EJtxRmU46C5geGunAOuSsQi43rnDn9vxCShq/KA5i9vuzE7rgb2bgmvhW3IJYZARyGgW5+Nlm4PjHYMtdvbAInpe3IrlMmvb77fbbuRjYv9ud4JRXIKO7wdmyYWDIjvRCrXm0eYvdmd89JPtNdJo8Bvd12EKbwCRubpoO0jrG3ys44LOb5hbX23W9ow7EmFX32IPsJPiUuw3FlhuR/N029rkKkPROohvq6OFjSZvOx33cntLb2Rh8LHMKuhxn6eOSUdCH2X/nujQnLftWZa2pLZuL4/Zgt5cBhrTnlgk1c8uo8wgNupbOEv3+cuktc1+8Hv/FHRug4v2f7OS7zypvaYXQedM7NBJZqaWJOjPjTOOJ9GMmDMazvzizle+SgwR9HjHyfd0dP7x4Dqt/e4MVMPZcWRuZvmT1rOy5ttekrDviTsvUgBu/oS1n5ezuZ+aGU08cVCy8dfNX995ETi3Zd6O+0iwQ56zP9izZBO3BHHg+PjF1nKP1QbsmXuUL5eJOUsnzWe4ljzi+Upqh7zf7lyz7PvvEK7WDP3GVsfE57FuuFWUlhU69XFGw7TT382aKN/BgWncEW/vf5s1YyZb0DkwogWCKW52twcBtE6cs3xrcBbt687ZDiIHUVf7zLptLWpX8efZM92ojFkEnd8/B0CcGUeJs7/VDgIaDeMAmUUMitRBfCBAzmzjnBVHiSZ5OnOyfugvksYxi6AnMUwbTNq/2zEZipTbrmt+o2NpUYltJ4wPdqJJo71MF7XLpDZ17PNtB+ysVhZf31nx3/qnoHP9M3JWyhNsxrd+RCG/dv981RUi6FP/Bdz0kc73ZbFSxHPoa8xZOk1Xx8h77bVhdo6Tzuh0juKg5K1nOp14OMump2nk2xDVD2eviYKeEEwonm8XF9uHIvLfoE/HYusYkzM97ukBzRSto8+ZjTocbJKYuUTGnk36hNq3GyLpfUkOYlwaefMJ47hG56gH/+Buq+S1+SnAIqsDCy9vZnb0c4msJnFB73Ay5Kzsu20nQ3v2HrXZuJNcnnaQFhQqq6C7TKgds+7W8oyr/TT3asccu2h6pyPVi+PNUljRsM1568A2WUc1y6Wu16YaHwmGq82zUyKLoCcxTNsamPZ7nnJ3fD+eeBi2M3Ek6Lb1hN/Gk1d2O4KyLukfEk3a7EFsvp6+31xdrqBnMQURDWeiNOlyhsG1r8hUnOrl3qp0moKjDzHLjCeqDp+gu8y9vqoMEfSkxlVE0Jm/NK/2eBmy7qu2R8Fcl7rrO9babsY27hN0moujCGy+gZmLi+1HQXMdze5HPW4+bA50OCLnWigTZ7I3HdX2jo9M91k6SZfI2ILgC/iTV9CZX854uL3IldhZcTnjWvo6tBK/K3Z0nKWnpXj7oz/Gki0P98jnID6DumxbM0uNr7unvYu/Zx14ZxV0F+88gs582cs5SeXggJfbIu/8RpZSmr6tSB00fWImWstoCa/jtt17f2zW1tNSFkFPYpgm2K7fi5SbvgFN/5NBbUtekuVhjuOmNQjP42AcsZKgp7Sa+PpgVg9MuyOkpyqjxSXN4DirSuoM6SgVxWSnEEfrp3kaOa/lXkc6R0UpbxS2uSboDWMGjDxiOaihj0Da7Ccqp53vpHWr6Drbu5kzFdbHnKWO1oE7zIcvcZZBh0QKaHzA9s7L7QGCb2DmEnR7ZknBGP9NYOvTDRe2xZs/3hZwdsyc5XAQwGQ7yqW1G5fI2E5VPqebrDP5eD5WPciEU6ZDVLSTJH5NtHQTXyKJruP39f6bZnbHpZrI+z/eudnOfVFdkFXkeGibV+M+GHxH1naQNoDqa0EnJ/pV0LeAbJrOlAkpLb4GbwmtAz5ju98CK+5lnPuSPN5dpul4lvtS0IuW29YB3/czxxLnEXTO0n2JsTWyOg2n9QcV/j1shs6C2WtWWWfL9pacaEsIn5V1hr7sdgA702Z8d08scnoEsyNkUI1xX+hcy6Qj3eXbGy9LepJGybeNKV6Rc0PQ2XBv+4JxIOPaZ+SJbns7pzW4rDPG+HokHYci72vX2rvr3UmizHXwyInJ9zzbHyLuKX7kNOO5zr+TQWTqj5z4IpMxudFcHQWoyGPVcIkMYyns+BfTDn0zftvZMetMNc5xxd0BCjy92CnwUWcf+QfQecs2H5MF11ptk3HH4USxsww6tnA1jEf3iJNapx/GvN/tGXredpCnbcYH2GmzRz477ww9nh9aRch66a1b7ap1gW9pKnpG3ISftw7ieeFJjivsDiyzFbDA8PavDCxEa4kv9aWgFy23PdD1fT9JJne7nn2xJNLaW81+Dxf0+PaxtL227HS2PLXdIT19PXD1Hm4v6CTxiTvFJZn6uS+S4hPtV6UVYM4+VhjnC3rIUmiOmtb+YLj2yo8ly57FuSHo9ki2Yx9+gkeqq7HGTanxwCu8L75Hl2Y+OsDZXsSuwQ9NaSNaznIzJgKXtiKWJa2hzzlhD0CSXwHzwoAbI79jShNfGuEpdKxrO1FkIvOwvc86uobPyOPx6lqiiG8vswOW2PmxPcKz7Gmm8xoFmrPeO77avV4e9zxn+19+53ZIZZf3fpLYufLJdsYZa9KgOd428rSDtA7UtxxUpqDz+z/4brOlkGZetof4KY62UGUZiNlhrfPUAQehrHNaC8mdFq14stfYs5zg2JeCXrTcWQaG9jZT28vdduT0BTRjTAruvGGfcOtnax/DP1zQKYh2tDg2RO5r5Ow3nmjW2/Tk9v5w21yeZf2YHxX3obNTTAofa7/PXhuLzMquwDL2DIrPyDrimxuCbs9aOmau8M8SbTZxk2mS2d3eTmZ3ZnbQFns7WPT8ZnuwttRFpm2np7G1Jc0VAIKiH0XCi5vmkgJa2I6C8bplPn3LDEmCY/Oynx1fckpasonv1462+viEzbaMxIPmRIMtMmkuF7XMkKvs3w4Qk+QoyT3e9CeIYjMkDZ64v71p9eFyjhUdL2kprWg7yCPo8XKUKehN61B0BoVjp8y6nwAYipgpi/XRHjzmqQO7DbuWEHe7oG1JzLI02JeCXrTc8YFhktmdgWPmREu0TO6cnNHHJ7JUJS2J2Ayy7t9Pa58V/z1c0FnAOFz+jeEXOVt/ZZLpeFbeuzvylV0JeQSdI+ltfg3QJBWlZmCZU4HZH5iTzhZbt/1b5OHsEnReGXeySAqwEa/MuS3ozE/c0S+LH0Nc0Pkc+jAwPOmMSaY+6ckcJfuZ7OS253JH65hcijq3r9FBi6Ebudc/Cixjr/e56jcuyHT2Yj0ypv0SG5o6ZmCXZkroeO3nRkFyor3vvKUj0EzrMXmdY+Izce6ImPGAcRK0zdjRYIHhchmoh+cCcLtRxCOrv0c8BgC/IwZ7oUMaY11v9KU2k0hoyMk+U4H3TPy5iYjHvbjNSFxWWFp2ntzvzO2bUUpcC7UCtNjtv2g7SOsQfazLFHT2Cfagn2310X8AtHZwxs7Bj113WQKTxCMeZq0DCrQdcZPmZwYtYjtKCgrksmTZbPtS0IuWm23P3hbJ/PO7YdhoLpWutHd7y1n0/duxTqIdLVG5n77WLDMNGWoiWUYxBPh70jbhtLbYD38vR9BZcEbmaR5gkRD3ugtMo2Xy/mhnZ5J0ZnFcNO3z0A+eACyVYJ6y32ePZn2C3mHaaY3I0xzN7M7HnsW6PqY0k2d8z2W0lOBzFGJZ7eAmWaKR5fFS5tph/Mhae/bua/SPXQDccIS5wlU2/tYRPMLzQFcs647Qqw3TEfKfKHWEgnUIlK8c8bbBa+3Qr/FQs4nPahgPZQ4C0lJ8GWjO9a3Id/b90dJRc+AyxR3DPemd8Xj0vCa+hOHbFlmkHaSV3cfaXjLL7eWecB66L368nU+KPaP/+bYK8vqQOrCDXkXvTgptG8XUiC8PxLn2paCHlJvlOGyy2WOfmmJbTWn92ud6YN6h/js5ubxs6+xOw6n5qO4F5Qk6y8gQnTv+zcysXIkdBDu1+NYL21TbnNl9zATM4EcXbW1KaszNw1n2SYgr3jCzPMYbjrZC2LP6eJQp5jc+M5pylvGUdiUG6djrSrP9zs6bvfZmRwaz18GSTET2DCnOINp3z5kh/QHsFBfotGhkdr75PM62k+KG07OZh0EkeSazw1j/M8kDOB7qwMMv/vOZdi5d9Rtd4Xte8zCbs92HmzQ9tBkQZJDZfx73gbBFil73PIAkzds6XufxQ4Ci5Z+oY933JrOOnZRodaD/AbfRZU38lvYYbYWNjd1IawQHTDcd0/6BDl3bnJF8YA73Vt9wGLDXVe2tUfagJHqK3Tb4t7RvIG87yFJ+F2t7u6prxmV/Y1HeXd8V88IlGQbp8R5m85PkA4qSyhJSB2yn/JZd8dzffNJ8A/+9KZ2iqw+y/57EMO13e6Bg3x9SbpZm53+aA5U6dhnQ0XW8+a6XZujmhNgRNNvvfytAv6qkxIEfNSBtAJROtF9cUa6gR0WmqXGtjwKLrGICjzCqGT3NaRJhzOCyU3TsXtQY+M68gRjKzlN/ex7NixQkMmRH//QNJma5L7GjZPxsmnUplDw5joOJLLPQpOfyeTw6sum1DrNUw723IcePllkP9H5mW6OYs6OIH8NpH+EYnUz2wl3dp1LlyRPj+y+zrQneQ/M6TxfjsggtEK5Byda/andw3BrI9eIoAiIZM3Y2j8/kMlXarDNLXstuB3xnGuss+cpzzRxmg9rHlnKJI+ko1izPLVoHUV/GgEAcCC6wlFm+fPLq9O8xS756fU3Rckf54o6jZgCk2cC0C8xkbI5p3YqeGS8H+y8GkeGAfp4FTX9kt/tel7siz++NoFekcMqGCIiACIhARQlwIMidBkNXNoMonj/Aw1XsZDuVxk94q2ix5ma2JOhzk77eLQIiIAIDmcCcOBJ0iHsWuGq39lo3rRU8PGto6xjcImGuBxhbCfoAq3AVVwREQAQqQ6ArHHfDeLpzuY1nQsxJjq2FlSlINTIiQa9GPSgXIiACIjAwCex9rTkO2Zka5jhZbrNU8hKQoKuBiIAIiIAIzF0CDGu8wQkmfgjPHGCiQxwdQBmmOcvRtXO3BJV4uwS9EtWgTIiACIiACIhAGAEJehg/3S0CIiACIiAClSAgQa9ENSgTIiACIiACIhBGQIIexk93i4AIiIAIiEAlCEjQK1ENyoQIiIAIiIAIhBGQoIfx090iIAIiIAIiUAkCEvRKVIMyIQIiIAIiIAJhBCToYfx0twiIgAiIgAhUgoAEvRLVoEyIgAiIgAiIQBgBCXoYP90tAiIgAiIgApUgIEGvRDUoEyIgAiIgAiIQRkCCHsZPd4uACIiACIhAJQhI0CtRDcqECIiACIiACIQRkKCH8dPdIiACIiACIlAJAhL0SlSDMiECIiACIiACYQQk6GH8dLcIiIAIiIAIVIKABL0S1aBMiIAIiIAIiEAYAQl6GD/dLQIiIAIiIAKVICBBr0Q1KBMiIAIiIAIiEEZAgh7GT3eLgAiIgAiIQCUISNArUQ3KhAiIgAiIgAiEEZCgh/HT3SIgAiIgAiJQCQIS9EpUgzIhAiIgAiIgAmEEJOhh/HS3CIiACIiACFSCgAS9EtWgTIiACIiACIhAGAEJehg/3S0CIiACIiAClSAgQa9ENSgTIiACIiACIhBGQIIexk93i4AIiIAIiEAlCEjQK1ENyoQIiIAIiIAIhBGQoIfx090iIAIiIAIiUAkCEvRKVIMyIQIiIAIiIAJhBCToYfx0twiIgAiIgAhUgoAEvRLVoEyIgAiIgAiIQBgBCXoYP90tAiIgAiIgApUgIEGvRDUoEyIgAiIgAiIQRkCCHsZPd4uACIiACIhAJQhI0CtRDcqECIiACIiACIQRkKCH8dPdIiACIiACIlAJAhL0SlSDMiECIiACIiACYQQk6GH8dLcIiIAIiIAIVIKABL0S1aBMiIAIiIAIiEAYAQl6GD/dLQIiIAIiIAKVICBBr0Q1KBMiIAIiIAIiEEZAgh7GT3eLgAiIgAiIQCUISNArUQ3KhAiIgAiIgAiEEZCgh/HT3SIgAiIgAiJQCQIS9EpUgzIhAiIgAiIgAmEEJOhh/HS3CIiACIiACFSCgAS9EtWgTIiACIiACIhAGAEJehg/3S0CIiACIiAClSAgQa9ENSgTIiACIiACIhBGQIIexk93i4AIiIAIiEAlCEjQK1ENyoQIiIAIiIAIhBGQoIfx090iIAIiIAIiUAkCEvRKVIMyIQIiIAIiIAJhBCToYfx0twiIgAiIgAhUgoAEvRLVoEyIgAiIgAiIQBgBCXoYP90tAiIgAiIgApUgIEGvRDUoEyIgAiIgAiIQRkCCHsZPd4uACIiACIhAJQhI0CtRDcqECIiACIiACIQRkKCH8dPdIiACIiACIlAJAhL0SlSDMiECIiACIiACYQQk6GH8dLcIiIAIiIAIVIKABL0S1aBMiIAIiIAIiEAYAQl6GD/dLQIiIAIiIAKVICBBr0Q1KBMiIAIiIAIiEEZAgh7GT3eLgAiIgAiIQCUISNArUQ3KhAiIgAiIgAiEEZCgh/HT3SIgAiIgAiJQCQIS9EpUgzIhAiIgAiIgAmEEJOhh/HS3CIiACIiACFSCgAS9EtWgTIiACIiACIhAGAEJehg/3S0CIiACIiAClSAgQa9ENSgTIiACIiACIhBGQIIexk93i4AIiIAIiEAlCEjQK1ENyoQIiIAIiIAIhBGQoIfx090iIAIiIAIiUAkCEvRKVIMyIQIiIAIiIAJhBCToYfx0twiIgAiIgAhUgoAEvRLVoEyIgAiIgAiIQBgBCXoYP90tAiIgAiIgy8X1DQAAAbZJREFUApUgIEGvRDUoEyIgAiIgAiIQRkCCHsZPd4uACIiACIhAJQhI0CtRDcqECIiACIiACIQRkKCH8dPdIiACIiACIlAJAhL0SlSDMiECIiACIiACYQQk6GH8dLcIiIAIiIAIVIKABL0S1aBMiIAIiIAIiEAYAQl6GD/dLQIiIAIiIAKVICBBr0Q1KBMiIAIiIAIiEEZAgh7GT3eLgAiIgAiIQCUISNArUQ3KhAiIgAiIgAiEEZCgh/HT3SIgAiIgAiJQCQIS9EpUgzIhAiIgAiIgAmEEJOhh/HS3CIiACIiACFSCgAS9EtWgTIiACIiACIhAGAEJehg/3S0CIiACIiAClSAgQa9ENSgTIiACIiACIhBGQIIexk93i4AIiIAIiEAlCEjQK1ENyoQIiIAIiIAIhBGQoIfx090iIAIiIAIiUAkCEvRKVIMyIQIiIAIiIAJhBCToYfx0twiIgAiIgAhUgoAEvRLVoEyIgAiIgAiIQBgBCXoYP90tAiIgAiIgApUgIEGvRDUoEyIgAiIgAiIQRkCCHsZPd4uACIiACIhAJQhI0CtRDcqECIiACIiACIQR+H/zySnyIbBX2wAAAABJRU5ErkJggg==";

    /*$scope.camlayout = true;
    $scope.SelectedImage = result;
    return false;*/
    basicComponent.displayLoadingDiv();
    pickImage.showActionsheet($scope).then(function(result) {
      //$scope.formObject = {};
      $scope.camlayout = true;
      $scope.SelectedImage = result;
      basicComponent.hideLoadingDiv();
    }).catch(function(e) {
      ionicToast.show("There is some problem with image, please try again.", 'bottom', false, 2000);
      basicComponent.hideLoadingDiv();
    });
  };

  //call time picker
  $scope.openTimePicker1 = function() {
    var nDate = new Date();
    remainder = 15 - nDate.getMinutes() % 15;
    nDate.setMinutes(nDate.getMinutes() + remainder);
    //set time object
    var ipObj1 = {
      inputTime: ((nDate.getHours() * 60 * 60) + (nDate.getMinutes() * 60)),
      callback: function(val) { //Mandatory
        if (typeof(val) === 'undefined') {
          console.log('Time not selected');
        } else {
          var selectedTime = new Date(val * 1000);
          console.log(selectedTime);
          console.log('Selected epoch is : ', val, 'and the time is ', selectedTime.getUTCHours(), 'H :', selectedTime.getUTCMinutes(), 'M');
          var minute = selectedTime.getUTCMinutes() >= 10 ? '' : '0';
          var hours = selectedTime.getUTCHours();
          if (minute === '0') {
            minute = minute + "" + selectedTime.getUTCMinutes();
          } else {
            minute = selectedTime.getUTCMinutes();
          }
          $scope.formObject.time = "from" + " " + hours + ":" + minute;
          $scope.formObject.formtime = hours + ":" + minute;
        }
      },
      format: 24, //Optional
      step: 15, //Optional
    };
    ionicTimePicker.openTimePicker(ipObj1);
  };


$scope.checkLanguage = function(){
  /*Date Picker Dutch*/
    ipObjdate = {
      callback: function(val) { //Mandatory
        console.log('In Dutch', val);
        var tempDate = $filter('date')(new Date(val), "LLLL dd yyyy");
        var temp =new Date(val);
        console.log(temp.getDate(), temp.getFullYear());


        if(tempDate.includes('January')){
          $scope.formObject.date = temp.getDate()+'.' + ' '+  'Jänner' + ' ' + temp.getFullYear();
        }
        else if(tempDate.includes('February')){
            $scope.formObject.date = temp.getDate()+'.' + ' '+  'Februar' + ' ' + temp.getFullYear();
        }
        else if(tempDate.includes('March')){
          $scope.formObject.date = temp.getDate()+'.' + ' '+  'März' + ' ' + temp.getFullYear();
        }
        else if(tempDate.includes('April')){
          $scope.formObject.date = temp.getDate()+'.' + ' '+  'April' + ' ' + temp.getFullYear();
        }
        else if(tempDate.includes('May')){
          $scope.formObject.date = temp.getDate()+'.' + ' '+  'Mai' + ' ' + temp.getFullYear();
        }
        else if(tempDate.includes('June')){
          $scope.formObject.date = temp.getDate()+'.' + ' '+  'Juni' + ' ' + temp.getFullYear();
        }
        else if(tempDate.includes('July')){
          $scope.formObject.date = temp.getDate()+'.' + ' '+  'Juli' + ' ' + temp.getFullYear();
        }
        else if(tempDate.includes('August')){
          $scope.formObject.date = temp.getDate()+'.' + ' '+  'August' + ' ' + temp.getFullYear();
        }
        else if(tempDate.includes('September')){
          $scope.formObject.date = temp.getDate()+'.' + ' '+  'September' + ' ' + temp.getFullYear();
        }
        else if(tempDate.includes('October')){
          $scope.formObject.date = temp.getDate()+'.' + ' '+  'Oktober' + ' ' + temp.getFullYear();
        }
        else if(tempDate.includes('November')){
          $scope.formObject.date = temp.getDate()+'.' + ' '+  'November' + ' ' + temp.getFullYear();
        }
        else if(tempDate.includes('December')){
          $scope.formObject.date = temp.getDate()+'.' + ' '+  'Dezember' + ' ' + temp.getFullYear();
        }

        //$scope.formObject.date = $filter('date')(new Date(val), "LLLL dd yyyy");
        console.log($scope.formObject.date, typeof($scope.formObject.date));
      },
      monthsList: ["Jänner", "Februar", "März", "April", "Mai", "Juni", "Juli", "August",
            "September", "Oktober", "November", "Dezember"],
    };
};





  /* open date picker for select date for project */
  $scope.openDatePicker = function() {
    ionicDatePicker.openDatePicker(ipObjdate);
  };

  //convert image after putting text on it
  $scope.convertImage = function(id) {
    document.getElementById(id).parentNode.style.overflow = 'visible'; //might need to do this to grandparent nodes as well, possibly.
    html2canvas([document.getElementById(id)], {
      scale: 2,
      onrendered: function(canvas) {
        document.getElementById(id).parentNode.style.overflow = 'hidden';
        var image = new Image();
        image.src = canvas.toDataURL("image/png");
        basicComponent.hideLoadingDiv();
        console.log(image.src);
        window.plugins.socialsharing.share(null, null, image.src, null);
        //Canvas2Image.saveAsPNG(canvas);
      }
    });
  };

  //invite via share image
  $scope.invite = function(inviteForm) {
    if (!inviteForm.$invalid) {
      basicComponent.displayLoadingDiv();
      $ionicScrollDelegate.$getByHandle('invite-content').scrollTop();
      $timeout(function() {
        if ($scope.camlayout) {
          $scope.convertImage('cameraLayout');
        } else {
          $scope.convertImage('layoutSelectedImage');
        }
      }, 250);
    } else {
      ionicToast.show('Please fill all fields.', 'bottom', false, 2000);
    }

  }



});
