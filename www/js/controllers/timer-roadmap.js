var App = angular.module('starter.timerroadmap', []);

App.controller('TimerRoadmapCtrl', function($scope, $rootScope, $ionicHistory, $stateParams, $state,
  ConnectivityMonitor, ionicToast, $ionicScrollDelegate, basicComponent, $timeout) {

  $scope.$on("$ionicView.beforeEnter", function() {

    $scope.title_image = '<img class="title_logo" src="img/home-logo.png"><h1 class="titletxt">my Active timers</h1>';
    $scope.border = {
      visible: false
    };
    console.log($rootScope.alignedTimer);
  });


  //Watch page scroll
  $scope.getScrollPosition = function() {
    var data = $ionicScrollDelegate.$getByHandle('timerRoad-content').getScrollPosition();
    if (data != null) {

      if (data.top > 0) {
        $scope.border.visible = true;
      } else {
        $scope.border.visible = false;
      }
      $scope.$apply();
    }
  };


  //Open timer on selected from list
  $scope.openTimer = function(timerIndex) {
    $rootScope.alignedTimer[timerIndex].isActive = true;
    basicComponent.displayLoadingDiv();
    $timeout(function() {
      if ($ionicHistory.backView().stateName === 'app.timer') {
        $ionicHistory.removeBackView();
      }
      $rootScope.lastIndex = timerIndex;
      $state.go('app.timer', {
        stateParam: timerIndex,
        location: "replace",
        reload: true
      });
      basicComponent.hideLoadingDiv();
    }, 500);
  };

});
