var App = angular.module('starter.home', []);

App.controller('HomeCtrl', function($rootScope, $scope, $stateParams, $state, ConnectivityMonitor, ionicToast, getData,
  basicComponent, $timeout, $ionicScrollDelegate) {

  var catPageCount;
  $scope.$on("$ionicView.beforeEnter", function() {
   $scope.title_image = '<img class="title_logo" src="img/about-logo.png">';
    $rootScope.getCategories();
    catPageCount = 2;
    $scope.noMoreCategories = false;
    $scope.border = {
      visible: false
    };
  });


  //Watch page scroll
  $scope.getScrollPosition = function() {
    var data = $ionicScrollDelegate.$getByHandle('home-content').getScrollPosition();
    if (data.top > 0) {
      $scope.border.visible = true;
    } else {
      $scope.border.visible = false;
    }
    $scope.$apply();
  };


  //load more category on page scroll
  $scope.loadMoreCategories = function() {
    if (ConnectivityMonitor.isOnline()) {
      getData.getAllCategories(catPageCount).then(function(result) {
        catPageCount++;
        if (result.data.status) {
          for (var i = 0; i < result.data.data.length; i++) {
            $scope.categories.splice(-1,1);
            $scope.categories.splice(-1,1);
            $scope.categories.push(result.data.data[i]);
            $scope.pushInviteBasic();
          }
          if ($scope.categories.length >= result.data.total_records) {
            $scope.noMoreCategories = true;
          }
        } else {
          $scope.noMoreCategories = true;
        }
        $scope.$broadcast('scroll.infiniteScrollComplete');
      }).catch(function(e) {
        ionicToast.show('Something went wrong. check network connection', 'bottom', false, 2000);
      });
    } else {
      ionicToast.show('Went offline. check network connection', 'bottom', false, 2000);
    }
  };

});
