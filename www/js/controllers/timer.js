var App = angular.module('starter.timer', []);

App.controller('TimerCtrl', function($rootScope, $scope, $stateParams, $state, ConnectivityMonitor, ionicToast, $timeout,
  $ionicScrollDelegate, pickImage, $ionicHistory, $ionicSlideBoxDelegate, $cordovaSocialSharing) {
  $scope.currentTimer = {};

  window.mukesh = $rootScope.alignedTimer;
  $scope.$on("$ionicView.beforeEnter", function() {


    cordova.plugins.backgroundMode.enable();

    cordova.plugins.backgroundMode.on('activate', function() {
      console.log('disableWebViewOptimizations');
       cordova.plugins.backgroundMode.disableWebViewOptimizations(); 
    });
    
    cordova.plugins.backgroundMode.setDefaults({
      title: 'PerfectBBQ',
      text: 'Your timer is running.',
      icon: 'icon',
      color: 'F14F4D',
      resume: true,
      hidden: false,
      bigText: false
    })
    cordova.plugins.backgroundMode.setDefaults({ silent: true });

    $rootScope.onTimerScreen = true;
    $scope.max = 100;
    $scope.border = {
      visible: false
    };

    $timeout(function() {
      $scope.cTop = 110 + document.getElementById('myDivTimer').offsetHeight;
      /*document.getElementById('cs2').style.fill = 'none';
      document.getElementById('cs2').style.opacity = '1';*/
    }, 10);


    $scope.setupTimer();
    if ($rootScope.lastIndex != null) {
      // if($rootScope.timerSlideIndex != undefined){
      $timeout(function() {
        // $ionicSlideBoxDelegate.slide($rootScope.timerSlideIndex);
        $ionicSlideBoxDelegate.slide($rootScope.lastIndex);
        $rootScope.alignedTimer[$rootScope.lastIndex].isActive = false;
        $ionicSlideBoxDelegate.update();
        $rootScope.alignedTimer[$rootScope.lastIndex].svgns = "http://www.w3.org/2000/svg";
        $rootScope.alignedTimer[$rootScope.lastIndex].noOfBreak = $rootScope.alignedTimer[$rootScope.lastIndex].recTimeInterval.length;
        $rootScope.alignedTimer[$rootScope.lastIndex].breakTimes = $rootScope.alignedTimer[$rootScope.lastIndex].recTimeInterval; //time slot in minute
        $rootScope.alignedTimer[$rootScope.lastIndex].newpercnt = 0;
        $scope.setupPoints($rootScope.lastIndex);
        $scope.currentTimer = $rootScope.alignedTimer[$rootScope.lastIndex];
        getIndex = $scope.getNextColumn($scope.currentTimer.timerCount, $scope.currentTimer);
        getCurrentIndex = $scope.getCurrentColumn($scope.currentTimer.timerCount, $scope.currentTimer);

        if (getIndex != undefined) {
          console.log($scope.currentTimer.breakTimes[getIndex]);
          $scope.currentTimer.nextStep = $scope.currentTimer.breakTimes[getIndex];
        } else {
          $scope.currentTimer.nextStep = {};
          $scope.currentTimer.nextStep.nextTime = 0;
        }

        if (getCurrentIndex != undefined) {
          $scope.currentTimer.cuurentStep = $scope.currentTimer.breakTimes[getCurrentIndex];
        } else {
          $scope.currentTimer.cuurentStep = {};
        }
      }, 200);
    } else {
      $timeout(function() {
        $ionicSlideBoxDelegate.slide($rootScope.alignedTimer.length - 1);
        $ionicSlideBoxDelegate.update();
        $rootScope.alignedTimer[$rootScope.alignedTimer.length - 1].svgns = "http://www.w3.org/2000/svg";
        $rootScope.alignedTimer[$rootScope.alignedTimer.length - 1].noOfBreak = $rootScope.alignedTimer[$rootScope.alignedTimer.length - 1].recTimeInterval.length;
        $rootScope.alignedTimer[$rootScope.alignedTimer.length - 1].breakTimes = $rootScope.alignedTimer[$rootScope.alignedTimer.length - 1].recTimeInterval; //time slot in minute
        $rootScope.alignedTimer[$rootScope.alignedTimer.length - 1].newpercnt = 0;
        $scope.setupPoints($rootScope.alignedTimer.length - 1);
        $scope.currentTimer = $rootScope.alignedTimer[$rootScope.alignedTimer.length - 1];

        getIndex = $scope.getNextColumn($scope.currentTimer.timerCount, $scope.currentTimer);
        getCurrentIndex = $scope.getCurrentColumn($scope.currentTimer.timerCount, $scope.currentTimer);

        if (getIndex != undefined) {
          $scope.currentTimer.nextStep = $scope.currentTimer.breakTimes[getIndex];
        } else {
          $scope.currentTimer.nextStep = {};
          $scope.currentTimer.nextStep.nextTime = 0;
        }

        if (getCurrentIndex != undefined) {
          $scope.currentTimer.cuurentStep = $scope.currentTimer.breakTimes[getCurrentIndex];
        } else {
          $scope.currentTimer.cuurentStep = {};
        }
      }, 200);
    }

    console.log($scope.currentTimer, $rootScope.alignedTimer);

  });

  $scope.$on("$ionicView.afterLeave", function() {
    $rootScope.onTimerScreen = false;
  });
  
  $rootScope.timeTick = []

  //start timer again
  $scope.makeAnother = function(index) {
    $scope.currentTimer = {};
    $timeout(function() {
      $rootScope.alignedTimer[index].timerCount = 0;
      $rootScope.alignedTimer[index].totalTime = (parseInt($rootScope.alignedTimer[index].recTimeMin) * 60) + parseInt($rootScope.alignedTimer[index].recTimeSec); //in second
      $rootScope.alignedTimer[index].countDown = angular.copy($rootScope.alignedTimer[index].totalTime - parseInt($rootScope.alignedTimer[index].timerCount));
      $rootScope.alignedTimer[index].countNew = angular.copy($rootScope.alignedTimer[index].totalTime - parseInt($rootScope.alignedTimer[index].timerCount));

      $rootScope.alignedTimer[index].nextStep = {};
      $rootScope.alignedTimer[index].cuurentStep = {};
      $rootScope.alignedTimer[index].nextStep.nextTime = 0;
      $rootScope.alignedTimer[index].started = false;
      $rootScope.alignedTimer[index].done = false;
      $rootScope.alignedTimer[index].paused = false;
      $scope.currentTimer = $rootScope.alignedTimer[index];
      $scope.currentTimer = $rootScope.alignedTimer[$rootScope.alignedTimer.length - 1];

      getIndex = $scope.getNextColumn($scope.currentTimer.timerCount, $scope.currentTimer);
      getCurrentIndex = $scope.getCurrentColumn($scope.currentTimer.timerCount, $scope.currentTimer);

      if (getIndex != undefined) {
        $scope.currentTimer.nextStep = $scope.currentTimer.breakTimes[getIndex];
      } else {
        $scope.currentTimer.nextStep = {};
        $scope.currentTimer.nextStep.nextTime = 0;
      }

      if (getCurrentIndex != undefined) {
        $scope.currentTimer.cuurentStep = $scope.currentTimer.breakTimes[getCurrentIndex];
      } else {
        $scope.currentTimer.cuurentStep = {};
      }
      $rootScope.alignedTimer[index].startTimer(index);
    }, 200);
  };

  //setup timer
  $scope.setupTimer = function() {
    for (var i = 0; i < $rootScope.alignedTimer.length; i++) {

      $rootScope.alignedTimer[i].isActive = false;
      $rootScope.alignedTimer[i].timerIndex = i;
      $rootScope.alignedTimer[i].totalTime = (parseInt($rootScope.alignedTimer[i].recTimeMin) * 60) + parseInt($rootScope.alignedTimer[i].recTimeSec); //in second
      $rootScope.alignedTimer[i].countDown = angular.copy($rootScope.alignedTimer[i].totalTime - parseInt($rootScope.alignedTimer[i].timerCount));
      $rootScope.alignedTimer[i].countNew = angular.copy($rootScope.alignedTimer[i].totalTime - parseInt($rootScope.alignedTimer[i].timerCount));
      $rootScope.alignedTimer[i].nextStep = {};
      $rootScope.alignedTimer[i].cuurentStep = {};
      $rootScope.alignedTimer[i].nextStep.nextTime = 0;
      $rootScope.alignedTimer[i].startTimer = function(index) {

        var vSlide = $rootScope.alignedTimer[index];
        // When you press a timer button this function is called
        vSlide.selectTimer = function(val) {
          vSlide.timerCount = 1;
          vSlide.countDown -= 1
        };
        // restart the current timer
        vSlide.reStartTimer = function(index) {
          vSlide.mytimeout = $timeout(vSlide.onTimeout, 1000);
          vSlide.started = true;
          vSlide.paused = false;
          document.getElementById('cs_svg_' + index).style.fill = 'none';
          document.getElementById('cs_svg_' + index).style.opacity = '1';
        };

        // stops and resets the current timer
        vSlide.stopTimer = function(index) {
          vSlide.timerCount = 0;
          vSlide.started = false;
          vSlide.paused = false;
          $timeout.cancel(vSlide.mytimeout);
          document.getElementById('cs_svg_' + index).style.fill = '#FFFFFF';
          document.getElementById('cs_svg_' + index).style.opacity = '0.4';
        };

        // pauses the timer
        vSlide.pauseTimer = function(index) {
          vSlide.started = false;
          vSlide.paused = true;
          $timeout.cancel(vSlide.mytimeout);
          document.getElementById('cs_svg_' + index).style.fill = '#FFFFFF';
          document.getElementById('cs_svg_' + index).style.opacity = '0.4';
        };

        // actual timer method, counts down every second, stops on zero
        vSlide.onTimeout = function() {
          /*console.log(vSlide.countDown);
          console.log('onTimeout', $rootScope.alignedTimer);*/
          if (vSlide != undefined) {
            if (vSlide.countDown != 0) {
              vSlide.countDown -= 1;
              $rootScope.timeTick[i] = vSlide.countDown;
            }

            if ((vSlide.nextStep.nextTime != 0 && vSlide.nextStep.nextTime == vSlide.timerCount && !$rootScope.onTimerScreen)) {
              var messageTitle = "";
              var messageText = "";
              if ($rootScope.language == 0) {
                messageTitle = vSlide.recTitleEng + " - Timer Alert";
                messageText = messageText + vSlide.nextStep.recIntervalTitleEng;
              } else {
                messageTitle = vSlide.recTitleDutch + " - Timer Alert";
                messageText = messageText + vSlide.nextStep.recIntervalTitleDutch;
              }
              sendLocalNotification(vSlide.recId, messageTitle, messageText);

            }
            getIndex = $scope.getNextColumn(vSlide.timerCount, vSlide);
            getCurrentIndex = $scope.getCurrentColumn(vSlide.timerCount, vSlide);

            if (getIndex != undefined) {
              vSlide.nextStep = vSlide.breakTimes[getIndex];
            } else {
              vSlide.nextStep = {};
              vSlide.nextStep.nextTime = 0;
            }

            if (getCurrentIndex != undefined) {
              vSlide.cuurentStep = vSlide.breakTimes[getCurrentIndex];
            } else {
              vSlide.cuurentStep = {};
            }

            if (vSlide.timerCount === vSlide.totalTime) {
              vSlide.done = true;
              vSlide.started = false;
              vSlide.paused = false;
              $timeout.cancel(vSlide.mytimeout);
              return;
            }
            vSlide.timerCount++;
            vSlide.mytimeout = $timeout(vSlide.onTimeout, 1000);
          }
        };

        vSlide.mytimeout = null;
        vSlide.mytimeout = $timeout(vSlide.onTimeout, 1000);
        vSlide.started = true;
        vSlide.selectTimer(vSlide.totalTime);
        getIndex = $scope.getNextColumn(vSlide.timerCount, vSlide);
        getCurrentIndex = $scope.getCurrentColumn(vSlide.timerCount, vSlide);
        if (getIndex != undefined) {
          vSlide.nextStep = vSlide.breakTimes[getIndex];
        } else {
          vSlide.nextStep = {};
          vSlide.nextStep.nextTime = 0;
        }

        if (getCurrentIndex != undefined) {
          vSlide.cuurentStep = vSlide.breakTimes[getCurrentIndex];
        } else {
          vSlide.cuurentStep = {};
        }
        $scope.slideChanged(index);
      };
    }
  }

  $scope.setupPoints = function(index) {
    //if ($rootScope.alignedTimer[index].started || ($rootScope.alignedTimer[index].paused && !$rootScope.alignedTimer[index].started)) {
    var breakTime = 0;
    for (var i = 0; i < $rootScope.alignedTimer[index].noOfBreak; i++) {
      $rootScope.alignedTimer[index].newpercnt = 0;
      breakTime = (parseInt($rootScope.alignedTimer[index].breakTimes[i].recIntervalTimeMin) * 60) + parseInt($rootScope.alignedTimer[index].breakTimes[i].recIntervalTimeSec)
      $rootScope.alignedTimer[index].breakTimes[i].nextTime = breakTime;
      $rootScope.alignedTimer[index].circlePercent = (breakTime * 100) / $rootScope.alignedTimer[index].totalTime;
      $rootScope.alignedTimer[index].newpercnt = $rootScope.alignedTimer[index].newpercnt + $rootScope.alignedTimer[index].circlePercent;
      var current = $rootScope.alignedTimer[index].newpercnt;
      var total = 100;
      var isSemicircle = false;
      var elementRadius = 100;
      var pathRadius = 94;
      var value = current > 0 ? Math.min(current, total) : 0;
      var type = isSemicircle ? 180 : 359.9999;
      var perc = total === 0 ? 0 : (value / total) * type;
      var per1 = total === 0 ? 0 : ((value + .66) / total) * type;
      var start = polarToCartesian(elementRadius, elementRadius, pathRadius, perc);
      var end = polarToCartesian(elementRadius, elementRadius, pathRadius, per1);
      var arcSweep = (perc <= 180 ? 0 : 1);
      var d = 'M ' + start.x + ' ' + start.y + ' A ' + pathRadius + ' ' + pathRadius + ' 0 ' + '0' + ' 1 ' + end.x + ' ' + end.y;
      $rootScope.alignedTimer[index].path = document.createElementNS($rootScope.alignedTimer[index].svgns, 'path');
      $rootScope.alignedTimer[index].path.setAttributeNS(null, "d", d);
      $rootScope.alignedTimer[index].path.setAttributeNS(null, 'stroke-width', '10px');
      //$rootScope.alignedTimer[index].path.style.stroke = "rgb(255, 237, 187)";
      $rootScope.alignedTimer[index].path.setAttributeNS(null, 'stroke', "rgb(255, 237, 187)");
      $rootScope.alignedTimer[index].path.setAttributeNS(null, 'stroke-linecap', 'circle');
      $rootScope.alignedTimer[index].path.setAttributeNS(null, 'fill', 'rgb(255, 237, 187)');
      document.getElementById('svg_' + index).appendChild($rootScope.alignedTimer[index].path);
    }

    if ($rootScope.alignedTimer[index].paused) {
      document.getElementById('cs_svg_' + index).style.fill = '#FFFFFF';
      document.getElementById('cs_svg_' + index).style.opacity = '0.4';
    } else {
      document.getElementById('cs_svg_' + index).style.fill = 'none';
      document.getElementById('cs_svg_' + index).style.opacity = '1';
    }
    //}

  }

  //calculate timer interval
  /*$scope.getNextColumn = function(timeConsumed, slide) {
    return_obj = {
      "prev_title": "",
      "next_title": ""
    };
    for (var i = 0; i < slide.noOfBreak; i++) {
      console.log(timeConsumed, slide.breakTimes[i].nextTime);
      if (timeConsumed < slide.breakTimes[i].nextTime) {
        return i;
      }
    }
  }*/
  $scope.getNextColumn = function(timeConsumed, slide) {
    return_obj = {
      "prev_title": "",
      "next_title": ""
    };
    for (var i = 0; i < slide.noOfBreak; i++) {
      if (timeConsumed <= (parseInt(slide.breakTimes[i].recIntervalTimeMin) * 60) + parseInt(slide.breakTimes[i].recIntervalTimeSec)) {
        return i;
      }
    }
  }

  $scope.getCurrentColumn = function(timeConsumed, slide) {
    return_obj = {
      "prev_title": "",
      "next_title": ""
    };
    for (var i = slide.noOfBreak - 1; i >= 0; i--) {
      if (timeConsumed >= (parseInt(slide.breakTimes[i].recIntervalTimeMin) * 60) + parseInt(slide.breakTimes[i].recIntervalTimeSec)) {
        return i;
      }
    }
  }

  //remove timer
  $scope.removeTimer = function(currentIndex) {
    $rootScope.alignedTimer.splice(currentIndex, 1);
    $timeout(function() {
      $scope.setupTimer();
      $ionicSlideBoxDelegate.update();
    }, 200);
    if ($rootScope.alignedTimer[currentIndex] == undefined && currentIndex == 0) {
      $scope.back();
    } else if ($rootScope.alignedTimer[currentIndex] == undefined && currentIndex != 0) {
      $timeout(function() {
        $ionicSlideBoxDelegate.slide(0);
        $scope.slideChanged(0);
      }, 200);
    } else {
      $timeout(function() {
        $ionicSlideBoxDelegate.slide(currentIndex);
        $scope.slideChanged(currentIndex);
      }, 200);
    }
  }

  //slide change update particular timer
  $scope.slideChanged = function(index) {
    $scope.title_image = '<img class="title_logo" src=' + $rootScope.alignedTimer[index].recImage + '>';
    // $rootScope.timerSlideIndex = index;
    $scope.setupPoints(index);
    $scope.currentTimer = $rootScope.alignedTimer[index];
    $rootScope.lastIndex = index;
    $scope.cTop = 110 + document.getElementById('myDivTimer').offsetHeight;
  }

  //watch page scroll
  $scope.getScrollPosition = function() {
    var data = $ionicScrollDelegate.$getByHandle('settimer-content').getScrollPosition();
    if (data.top > 0) {
      $scope.border.visible = true;
    } else {
      $scope.border.visible = false;
    }
    $scope.$apply();
  };

  //calculate x and y point of timer interval
  function polarToCartesian(centerX, centerY, radius, angleInDegrees) {
    var newObj = {};
    var angleInRadians = (angleInDegrees - 90) * Math.PI / 180.0;
    var x = centerX + (radius * Math.cos(angleInRadians));
    var y = centerY + (radius * Math.sin(angleInRadians));
    newObj.x = x;
    newObj.y = y;
    return newObj;
  }

  // This function helps to display the time in a correct way in the center of the timer
  $scope.humanizeDurationTimer = function(input, units) {
    // units is a string with possible values of y, M, w, d, h, m, s, ms
    if (input == 0) {
      return 0;
    } else {
      var duration = moment().startOf('day').add(input, units);
      var format = "";
      if (duration.hour() > 0) {
        format += "H[h] ";
      }
      if (duration.minute() > 0) {
        format += "m[m] ";
      }
      if (duration.second() > 0) {
        format += "s[s] ";
      }
      return duration.format(format);
    }
  };

  $scope.invite = function(index) {
    $state.go("app.invite");
  }

  //Share on complete timer
  $scope.share = function() {
    pickImage.showActionsheet($scope).then(function(result) {
      $scope.image = result;
      window.plugins.socialsharing.share(null, 'Perfect BBQ', result, null);
    }).catch(function(e) {
      ionicToast.show("There is some problem with image, please try again.", 'bottom', false, 2000);
    });
  };

  //send local notification about timer
  function sendLocalNotification(id, notificationTitle, notificationText) {
    var now = new Date();
    cordova.plugins.notification.local.schedule({
      id: now.getTime(),
      title: notificationTitle,
      text: notificationText,
      at: new Date(now.getTime() + 1 * 1000),
      data: {}
    }, function() {
      console.log("function called.");
    });
  };

});
