var App = angular.module('starter.setparameter', []);

App.controller('SetParameterCtrl', function($rootScope, $scope, $stateParams, $state, ConnectivityMonitor, 
  ionicToast, $ionicScrollDelegate, recipeWS, basicComponent,$timeout) {

  $scope.$on("$ionicView.beforeEnter", function() {
    $scope.titleObject = $stateParams.stateParam;
    $scope.title_image = '<img class="title_logo" src=' + $scope.titleObject.recImage + '>';
    if ($scope.range1 == undefined && $scope.range2 == undefined) {
      $scope.range1 = "";
      $scope.range2 = "";
      $scope.rangeSlider = {
        range1: false,
        range2: false
      };
      if ($scope.titleObject.recTempMode == 'yes') {
        $scope.rangeSlider.range2 = true;
        $scope.range2 = 5;
      }
      if ($scope.titleObject.recThickMode == 'yes') {
        $scope.rangeSlider.range1 = true;
        $scope.range1 = 1;
      }
    }
    $scope.border = {
      visible: false
    };

    $timeout( function(){
      $scope.cTop = 120+document.getElementById('myDivSetpera').offsetHeight;
    }, 10 );
  });

  //set range 1
  $scope.range1Change = function(range) {
    //$ionicScrollDelegate.$getByHandle('setpara-content').freezeScroll(true);
    $scope.range1 = (range / 2) + 0.5;
  };


  //set range 2
  $scope.range2Change = function(range) {
    //$ionicScrollDelegate.$getByHandle('setpara-content').freezeScroll(true);
    $scope.range2 = 6 - range;
  };

  $scope.touchStart = function() {
    $ionicScrollDelegate.$getByHandle('setpara-content').freezeScroll(true);
    console.log('touch start');
  }

  $scope.touchRelease = function() {
    $ionicScrollDelegate.$getByHandle('setpara-content').freezeScroll(false);
    console.log('touch release');
  }

  //Watch page scroll
  $scope.getScrollPosition = function() {
    var data = $ionicScrollDelegate.$getByHandle('setpara-content').getScrollPosition();
    if (data.top > 0) {
      $scope.border.visible = true;
    } else {
      $scope.border.visible = false;
    }
    $scope.$apply();
  };

  //GEt recipe timing details
  $scope.getRecipeTiming = function() {
    if ($rootScope.alignedTimer.length < 4) {
      if (ConnectivityMonitor.isOnline()) {
        basicComponent.displayLoadingDiv();
        recipeWS.getRecipeTiming($scope.titleObject.recId, $scope.range1, $scope.range2).then(function(result) {
          basicComponent.hideLoadingDiv();
          if (result.data.status) {
            if (result.data.data.recThickness != null) {
              result.data.data.recThickness = result.data.data.recThickness.replace('cm', '');
            } else {
              result.data.data.recThickness = 0;
            }
            result.data.data.timerCount = 0;
            if (result.data.data.recCookingStyle != null) {
              result.data.data.recCookingStyle = result.data.data.recCookingStyle.split(" ").join("<br />");
            }
            result.data.data.timerIndex = $rootScope.alignedTimer.length;
            $rootScope.alignedTimer.push(result.data.data);
            $rootScope.lastIndex = $rootScope.alignedTimer.length - 1;
            $state.go('app.timer', {
              stateParam: $rootScope.alignedTimer.length - 1
            });
          }
        }).catch(function(e) {
          ionicToast.show('Something went wrong. check network connection', 'bottom', false, 2000);
        });
      } else {
        ionicToast.show('Went offline. check network connection', 'bottom', false, 2000);
      }
    } else {
      ionicToast.show('You have already added maximum 4 timers.', 'bottom', false, 2000);
    }
  };

});
