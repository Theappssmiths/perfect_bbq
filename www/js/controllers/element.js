var App = angular.module('starter.element', []);

App.controller('ElementCtrl', function($rootScope, $scope, $stateParams, $state, ConnectivityMonitor,
        ionicToast, $ionicScrollDelegate,getData, basicComponent,$timeout) {

  var catSubPageCount;
  var intervalListener;
  $scope.$on("$ionicView.beforeEnter", function() {
    $scope.layout = {
      oneItemLayout: false,
      twoItemLayout: true
    };
    $scope.border = {
      visible: false
    };
    catSubPageCount = 2;
    $scope.titleObject = $scope.selectedSubCategory;
    $scope.title_image = '<img class="title_logo" src=' + $scope.titleObject.catImage + '>';
    $scope.startTimer();

    $timeout( function(){
      $scope.cTop = 110+document.getElementById('myDiv').offsetHeight;
      console.log(document.getElementById('myDiv').offsetHeight);
    }, 10 );

  });

  //advertisement event listener
  $scope.$on("$ionicView.afterLeave", function() {
    //window.clearInterval(intervalListener);
  });

  $scope.$on("$ionicView.beforeLeave", function() {
    //window.clearInterval(intervalListener);
  });


  //Watch page scroll
  $scope.getScrollPosition = function() {
    var data = $ionicScrollDelegate.$getByHandle('element-content').getScrollPosition();
    if (data.top > 0) {
      $scope.border.visible = true;
    } else {
      $scope.border.visible = false;
    }
    $scope.$apply();
  };

  //start advertisement timer
  $rootScope.startTimer = function() {
    if($rootScope.catAdv.length > 0){
      //intervalListener = setTimeout(function() {
        $rootScope.adv = $rootScope.catAdv[Math.floor(Math.random() * $rootScope.catAdv.length)];
        $scope.showAd();
      //}, 30000);
    }
  };

  //load more sub category on page scroll
  $scope.loadMoreSubCategories = function() {
    if (ConnectivityMonitor.isOnline()) {
      getData.getAllSubCategories($scope.selectedSubCategory.catId, catSubPageCount).then(function(result) {
        catSubPageCount++;
        if (result.data.status) {
          for (var i = 0; i < result.data.data.length; i++) {
            $scope.subCategories.push(result.data.data[i]);
          }
          if ($scope.subCategories.length >= result.data.total_records) {
            $scope.noMoreSubCategories = true;
          }
        } else {
          $scope.noMoreSubCategories = true;
        }
        $scope.$broadcast('scroll.infiniteScrollComplete');
      }).catch(function(e) {
        ionicToast.show('Something went wrong. check network connection', 'bottom', false, 2000);
      });
    } else {
      ionicToast.show('Went offline. check network connection', 'bottom', false, 2000);
    }
  };

});
