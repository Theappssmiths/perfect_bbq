var App = angular.module('starter.bbqbasic', []);

App.controller('BBQBasicCtrl', function($scope, $rootScope, $stateParams,$state,ConnectivityMonitor,ionicToast,$ionicScrollDelegate,
										getData,basicComponent,$timeout) {

	var basicPageCount;
	$scope.$on("$ionicView.beforeEnter", function () {
		$scope.border = {visible : false};
		basicPageCount = 2;
		$scope.title_image ='<img class="title_logo" src="img/basic.png">';

		$timeout( function(){
            $scope.cTop = 110+document.getElementById('myDivbbqlist').offsetHeight;
            $scope.cTopdetail = 110+document.getElementById('myDivbbqlistdetail').offsetHeight;

      	}, 10 );
		
  });

	//watch page scroll
	$scope.getScrollPositionList= function(){
		var data =  $ionicScrollDelegate.$getByHandle('bbqbasiclist-content').getScrollPosition();
		console.log(data);
		if(data.top > 0){
			$scope.border.visible = true;
		}else{
			$scope.border.visible = false;
			console.log('else',$scope.border.visible);
		}
		$scope.$apply();
	};

	//watch page scroll
	$scope.getScrollPositionDetail= function(){
		var data =  $ionicScrollDelegate.$getByHandle('bbqbasicdetail-content').getScrollPosition();
		console.log(data);
		if(data.top > 0){
			$scope.border.visible = true;
		}else{
			$scope.border.visible = false;
			console.log('else',$scope.border.visible);
		}
		$scope.$apply();
	};

 	//Get BBQ basic detail
	$scope.goToBBQBasicDetail = function(item){
		if (ConnectivityMonitor.isOnline()) {
			basicComponent.displayLoadingDiv();
			getData.getBBQDetail(item.bbqId).then(function(result) {
				basicComponent.hideLoadingDiv();
				if (result.data.status) {
					$rootScope.basicDetails = result.data.data;
					$rootScope.basicDetails.bbqTitleImage = '<img class="title_logo" src=' + result.data.data.bbqImage + '>';
					console.log($rootScope.basicDetails);
					$state.go('app.bbqbasicdetail');
				}
			}).catch(function(e) {
				basicComponent.hideLoadingDiv();
				ionicToast.show('Something went wrong. check network connection', 'bottom', false, 2000);
			});
		} else {
			ionicToast.show('Went offline. check network connection', 'bottom', false, 2000);
		}
	};

	//Load more BBQ basics on page scroll
	$scope.loadMorebbqBasics = function() {
	    if (ConnectivityMonitor.isOnline()) {
	      getData.getBBQBasics(basicPageCount).then(function(result) {
	        basicPageCount++;
	        if (result.data.status) {
	          for (var i = 0; i < result.data.data.length; i++) {
	            $scope.bbqBasics.push(result.data.data[i]);
	          }
	          if ($scope.bbqBasics.length >= result.data.total_records) {
	            $scope.noMoreBasics = true;
	          }
	        } else {
	          $scope.noMoreBasics = true;
	        }
	        $scope.$broadcast('scroll.infiniteScrollComplete');
	      }).catch(function(e) {
	        ionicToast.show('Something went wrong. check network connection', 'bottom', false, 2000);
	      });
	    } else {
	      ionicToast.show('Went offline. check network connection', 'bottom', false, 2000);
	    }
	};

});
