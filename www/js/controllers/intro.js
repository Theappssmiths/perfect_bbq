var App = angular.module('starter.intro', []);

App.controller('IntroCtrl', function($scope, $stateParams,$state,ConnectivityMonitor,ionicToast,
									$ionicSlideBoxDelegate,LS,$rootScope,$rootScope) {

	$scope.$on("$ionicView.beforeEnter", function () {
		$scope.title_image ='<img class="title_logo" src="img/about-logo.png">';
		$scope.slide = {oneyes:true, twoyes:false, threeyes:false, fouryes:false, fiveyes:false};
		$ionicSlideBoxDelegate.update();
		$ionicSlideBoxDelegate.slide(0);
		if($rootScope.hideSlide){
			$scope.slide = {oneyes:true, twoyes:false, threeyes:false, fouryes:false, fiveyes:false};
		}else{
			$scope.slide = {oneyes:false, twoyes:true, threeyes:false, fouryes:false, fiveyes:false};
		}
  });

	  // Called to navigate to the main app
	  $scope.startApp = function() {
	    $state.go('main');
	  };

	  //Go to next slide
	  $scope.next = function() {
	    $ionicSlideBoxDelegate.next();
	  };

	  //Go to previous slide
	  $scope.previous = function() {
	    $ionicSlideBoxDelegate.previous();
	  };

	  // Called each time the slide changes
	  $scope.slideChanged = function(index) {
	    $scope.slideIndex = index;
	    if(!$rootScope.hideSlide){
	    	index = index+1;
	   	}
	    if(index==0){
	    	$scope.slide = {oneyes:true, twoyes:false, threeyes:false, fouryes:false, fiveyes:false};
	    }else if(index==1){
	    	$scope.slide = {oneyes:true, twoyes:true, threeyes:false, fouryes:false, fiveyes:false};
	    }else if(index ==2){
	    	$scope.slide = {oneyes:true, twoyes:true, threeyes:true, fouryes:false, fiveyes:false};
	    }else if(index == 3){
	    	$scope.slide = {oneyes:true, twoyes:true, threeyes:true, fouryes:true, fiveyes:false};
	    }else if (index == 4){
	    	$scope.slide = {oneyes:true, twoyes:true, threeyes:true, fouryes:true, fiveyes:true};
	    }
	  };

	  //Navigate to home screen
	  $scope.goToHome = function(){
	  	if($scope.languages.checked == false){
	  		LS.setData("bbqLanguage", 1);
	  		$rootScope.language = 1;
	  	}else{
	  		LS.setData("bbqLanguage", 0);
	  		$rootScope.language = 0;
	  	}
	  	$state.go('app.home');
	  	$rootScope.getCategories();
	  }

	//Set language 
	$scope.selectedLanguage = function(){
		if($scope.languages.checked){
			$rootScope.language = 0;
			LS.setData("bbqLanguage", 0); 
		}else{
			$rootScope.language = 1;
			LS.setData("bbqLanguage", 1);
		}
	};
});
