angular.module('starter.preparationscreen', [])

  .controller('PreparationScreenCtrl', function($scope, $state, ConnectivityMonitor, $stateParams, $ionicScrollDelegate,
    recipeWS, basicComponent, ionicToast, $rootScope,$timeout) {

    $scope.$on('$ionicView.enter', function(e) {
      $scope.titleObject = $rootScope.recDetail;
      console.log($scope.titleObject);
      $scope.title_image = '<img class="title_logo" src=' + $scope.titleObject.recImage + '>';
      $scope.border = {
        visible: false
      };

      $timeout( function(){
            $scope.cTop = 110+document.getElementById('myDivprep').offsetHeight;
            console.log(document.getElementById('myDivprep').offsetHeight);
      }, 10 );

    });

    //Watch page scroll
    $scope.getScrollPosition = function() {
      
      var data = $ionicScrollDelegate.$getByHandle('prepscreen-content').getScrollPosition();
      if (data.top > 0) {
        $scope.border.visible = true;
      } else {
        $scope.border.visible = false;
      }
      $scope.$apply();
    };


    //set perameter for timer like temp or thickness
    $scope.setParameter = function() {
      console.log($rootScope.recDetail);
      if ($rootScope.alignedTimer.length < 4) {
        console.log($rootScope.recDetail);
        if ($rootScope.recDetail.recTempMode === "yes" || $rootScope.recDetail.recThickMode === "yes") {
          $state.go('app.setparameter', {
            stateParam: $rootScope.recDetail
          });
        } else {
          if (ConnectivityMonitor.isOnline()) {
            basicComponent.displayLoadingDiv();
            recipeWS.getRecipeTiming($rootScope.recDetail.recId, "", "").then(function(result) {
              basicComponent.hideLoadingDiv();
              if (result.data.status) {
                if (result.data.data.recThickness != null) {
                  result.data.data.recThickness = result.data.data.recThickness.replace('cm', '');
                } else {
                  result.data.data.recThickness = 0;
                }
                result.data.data.timerCount = 0;
                if (result.data.data.recCookingStyle != null) {
                  result.data.data.recCookingStyle = result.data.data.recCookingStyle.split(" ").join("<br />");
                }
                result.data.data.timerIndex = $rootScope.alignedTimer.length;
                $rootScope.alignedTimer.push(result.data.data);
                $rootScope.lastIndex = $rootScope.alignedTimer.length - 1;
                $state.go('app.timer', {
                  stateParam: $rootScope.alignedTimer.length - 1
                });
              }
            }).catch(function(e) {
              ionicToast.show('Something went wrong. check network connection', 'bottom', false, 2000);
            });
          } else {
            ionicToast.show('Went offline. check network connection', 'bottom', false, 2000);
          }
        }
      } else {
        ionicToast.show('You have already added maximum 4 timers.', 'bottom', false, 2000);
      }
    }

  });
