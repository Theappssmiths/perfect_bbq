var App = angular.module('starter.directive', []);

//Alow Input only numbers Directive
App.directive('numbers', function(){
   return {
     require: 'ngModel',
     link: function(scope, element, attrs, modelCtrl) {
       modelCtrl.$parsers.push(function (inputValue) {
           if (inputValue === undefined) return '';
           var transformedInput = inputValue.replace(/[^0-9]/g, '');
           if (transformedInput!=inputValue) {
              modelCtrl.$setViewValue(transformedInput);
              modelCtrl.$render();
           }

           return transformedInput;
       });
     }
   };
});


 App.directive('selectOnClick', ['$window', function ($window) {
    // Linker function
    return function (scope, element, attrs) {
      element.bind('click', function () {
        if (!$window.getSelection().toString()) {
          this.setSelectionRange(0, this.value.length)
        }
      });
    };
  }]);
 
App.filter('secondsToDateTime', [function() {
    return function(seconds) {
        return new Date(1970, 0, 1).setSeconds(seconds);
    };
}]);

App.filter('capitalize', function() {
    return function(input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});


App.filter('capitalizee', function() {
    return function(input) {
      return (!!input) ? input.toUpperCase() : '';
    }
});

//Directive thatrestrict input length  my-maxlength="15"
App.directive('myMaxlength', function() {
  return {
    require: 'ngModel',
    link: function (scope, element, attrs, ngModelCtrl) {
        var maxlength = Number(attrs.myMaxlength);
        function fromUser(text) {
            if (text.length > maxlength) {
              var transformedInput = text.substring(0, maxlength);
              ngModelCtrl.$setViewValue(transformedInput);
              ngModelCtrl.$render();
              return transformedInput;
            }
            return text;
        }
        ngModelCtrl.$parsers.push(fromUser);
    }
  };
});


//exec-on-scroll-to-top
App.directive('execOnScrollToTop', function () {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      var fn = scope.$eval(attrs.execOnScrollToTop);
      element.on('scroll', function (e) {
        if (!e.target.scrollTop) {
          scope.boolChangeClass = true;
          scope.$apply(fn);
        }
      });
    }
  };
});
