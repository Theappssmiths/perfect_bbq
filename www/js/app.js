angular.module('starter', ['ionic', 'ngCordova', 'starter.controllers', 'starter.networkService',
    'starter.directive', 'starter.home', 'ionic-toast', 'starter.selectedelement',
    'starter.preparationscreen', 'starter.element', 'starter.timerroadmap',
    'starter.setparameter', "kendo.directives", 'starter.timer',
    'angular-svg-round-progressbar', 'starter.intro', 'starter.commonService',
    'starter.getdataService', 'starter.invite', 'starter.recipeWS', 'starter.setting',
    'starter.bbqbasic', 'ionic-timepicker', 'ionic-datepicker','ngSanitize'
  ])

  .run(function($ionicPlatform, $state, ConnectivityMonitor, $rootScope, $cordovaStatusbar) {
    $ionicPlatform.ready(function() {
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
        cordova.plugins.Keyboard.disableScroll(true);
      }
      // if (window.StatusBar) {
      //   StatusBar.overlaysWebView(false);
      //   StatusBar.backgroundColorByHexString("#FDE3A5");
      // }
    });
  })

  .config(function($httpProvider) {
    $httpProvider.defaults.headers.common = {};
    $httpProvider.defaults.headers.post = {};
    $httpProvider.defaults.headers.put = {};
    $httpProvider.defaults.headers.patch = {};
  })

  .config(function(ionicTimePickerProvider) {
    var timePickerObj = {
      inputTime: (((new Date()).getHours() * 60 * 60) + ((new Date()).getMinutes() * 60)),
      format: 12,
      step: 15,
      setLabel: 'Set',
      closeLabel: 'Close'
    };
    ionicTimePickerProvider.configTimePicker(timePickerObj);
  })

  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider

      .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html'
      })

      .state('intro', {
        url: '/intro',
        templateUrl: 'templates/intro-screen.html',
        controller: 'IntroCtrl'
      })

      .state('app.setting', {
        url: '/setting',
        views: {
          'menuContent': {
            templateUrl: 'templates/setting.html',
            controller: 'SettingCtrl'
          }
        }
      })


      .state('app.about', {
        url: '/about',
        views: {
          'menuContent': {
            templateUrl: 'templates/about.html',
            controller: ''
          }
        }
      })

      .state('app.element', {
        url: '/element',
        params: {
          stateParam: null
        },
        views: {
          'menuContent': {
            templateUrl: 'templates/element.html',
            controller: 'ElementCtrl'
          }
        }
      })

      .state('app.selectedElement', {
        url: '/selectedElement',
        params: {
          stateParam: null
        },
        views: {
          'menuContent': {
            templateUrl: 'templates/selected-element.html',
            controller: 'SelectedelEmentCtrl'
          }
        }
      })

      .state('app.preparationScreen', {
        url: '/preparationScreen',
        cache: false,
        params: {
          stateParam: null
        },
        views: {
          'menuContent': {
            templateUrl: 'templates/preparation-screen.html',
            controller: 'PreparationScreenCtrl'
          }
        }
      })

      .state('app.timerroadmap', {
        url: '/timerroadmap',
        params: {
          stateParam: null
        },
        views: {
          'menuContent': {
            templateUrl: 'templates/timer-roadmap.html',
            controller: 'TimerRoadmapCtrl'
          }
        }
      })

      .state('app.setparameter', {
        url: '/setparameter',
        params: {
          stateParam: null
        },
        views: {
          'menuContent': {
            templateUrl: 'templates/set-parameter.html',
            controller: 'SetParameterCtrl'
          }
        }
      })

      .state('app.timer', {
        url: '/timer',
        cache: false,
        params: {
          stateParam: null
        },
        views: {
          'menuContent': {
            templateUrl: 'templates/timer.html',
            controller: 'TimerCtrl'
          }
        }
      })

      .state('app.invite', {
        url: '/invite',
        params: {
          stateParam: null
        },
        views: {
          'menuContent': {
            templateUrl: 'templates/invite.html',
            controller: 'InviteCtrl'
          }
        }
      })

      .state('app.bbqbasiclist', {
        url: '/bbqbasiclist',
        params: {
          stateParam: null
        },
        views: {
          'menuContent': {
            templateUrl: 'templates/bbqbasiclist.html',
            controller: 'BBQBasicCtrl'
          }
        }
      })

      .state('app.bbqbasicdetail', {
        url: '/bbqbasicdetail',
        cache: false,
        params: {
          stateParam: null
        },
        views: {
          'menuContent': {
            templateUrl: 'templates/bbqbasicdetail.html',
            controller: 'BBQBasicCtrl'
          }
        }
      })

      .state('app.home', {
        url: '/home',
        views: {
          'menuContent': {
            templateUrl: 'templates/home.html',
            controller: 'HomeCtrl'
          }
        }
      });


    // if none of the above states are matched, use this as the fallback
    //$urlRouterProvider.otherwise('app/invite');
  });
