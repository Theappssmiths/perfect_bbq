var app = angular.module('starter.recipeWS', ['ionic']);

app.factory("recipeWS", function($window, $q, $http) {
  return {
    getRecipes: function(catid, page) {
      var q = $q.defer();
      $http({
        method: 'GET',
        timeout: 20000,
        withCredentials: false,
        url: "http://admin.perfectbbqapp.com/api/recipe/recipeByCategory?categoryId=" + catid + "&page=" + page,
      }).then(function successCallback(response) {
        q.resolve(response);
      }, function errorCallback(response) {
        q.reject(response);
      });
      return q.promise;
    },
    getRecipeDetail: function(recipeID) {
      var q = $q.defer();
      $http({
        method: 'GET',
        timeout: 20000,
        withCredentials: false,
        url: "http://admin.perfectbbqapp.com/api/recipe/recipeDetail?recipeId=" + recipeID,
      }).then(function successCallback(response) {
        q.resolve(response);
      }, function errorCallback(response) {
        q.reject(response);
      });
      return q.promise;
    },
    getRecipeTiming: function(recipeId, thick, style) {
      var q = $q.defer();
      $http({
        method: 'GET',
        timeout: 20000,
        withCredentials: false,
        url: "http://admin.perfectbbqapp.com/api/recipe/recipeTiming?recipeId=" + recipeId + "&thickness=" + thick + "&cookStyle=" + style,
      }).then(function successCallback(response) {
        q.resolve(response);
      }, function errorCallback(response) {
        q.reject(response);
      });
      return q.promise;
    },
  }
})
