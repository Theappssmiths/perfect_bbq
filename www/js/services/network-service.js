var network = angular.module('starter.networkService', ['ionic']);

network.factory('ConnectivityMonitor', function($rootScope, $cordovaNetwork) {

  return {
    isOnline: function() {
      if (ionic.Platform.isWebView()) {
        return $cordovaNetwork.isOnline();
      } else {
        return navigator.onLine;
      }
    },
    ifOffline: function() {
      if (ionic.Platform.isWebView()) {
        return !$cordovaNetwork.isOnline();
      } else {
        return !navigator.onLine;
      }
    },
    startWatching: function() {
      if (ionic.Platform.isWebView()) {
        $rootScope.$on('$cordovaNetwork:online', function(event, networkState) {});
        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState) {});
      } else {
        window.addEventListener("online", function(e) {}, false);
        window.addEventListener("offline", function(e) {}, false);
      }
    }
  }
})
