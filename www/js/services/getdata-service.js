var app = angular.module('starter.getdataService', ['ionic']);

app.factory("getData", function($window,$q,$http) {
  return {
    getAllCategories: function(page) {
      var q = $q.defer();
      $http({
        method: 'GET',
        timeout: 20000,
        withCredentials: false,
        url: "http://admin.perfectbbqapp.com/api/category/allCategory?page=" + page,
      }).then(function successCallback(response) {
        q.resolve(response);
      }, function errorCallback(response) {
        q.reject(response);
      });
      return q.promise;
    },
    getStaticDetail: function(id) {
      var q = $q.defer();
      $http({
        method: 'GET',
        timeout: 20000,
        withCredentials: false,
        url: "http://admin.perfectbbqapp.com/api/page/pageDetail?id=" + id,
      }).then(function successCallback(response) {
        q.resolve(response);
      }, function errorCallback(response) {
        q.reject(response);
      });
      return q.promise;
    },
    getAllSubCategories: function(catid,page) {
      var q = $q.defer();
      $http({
        method: 'GET',
        timeout: 20000,
        withCredentials: false,
        url: "http://admin.perfectbbqapp.com/api/category/allSubCategory?parentCatId="+catid+"&page="+page,
      }).then(function successCallback(response) {
        q.resolve(response);
      }, function errorCallback(response) {
        q.reject(response);
      });
      return q.promise;
    },
    searchRecipes: function(keyword,page) {
      var q = $q.defer();
      $http({
        method: 'GET',
        timeout: 20000,
        withCredentials: false,
        url: "http://admin.perfectbbqapp.com/api/search/byKeyword?keyword="+keyword+"&page="+page,
      }).then(function successCallback(response) {
        q.resolve(response);
      }, function errorCallback(response) {
        q.reject(response);
      });
      return q.promise;
    },
    getBBQBasics: function(page) {
      var q = $q.defer();
      $http({
        method: 'GET',
        timeout: 20000,
        withCredentials: false,
        url: "http://admin.perfectbbqapp.com/api/basic/allBasic?page=" + page,
      }).then(function successCallback(response) {
        q.resolve(response);
      }, function errorCallback(response) {
        q.reject(response);
      });
      return q.promise;
    },
    getBBQDetail: function(id) {
      var q = $q.defer();
      $http({
        method: 'GET',
        timeout: 20000,
        withCredentials: false,
        url: "http://admin.perfectbbqapp.com/api/basic/basicDetail?id=" + id,
      }).then(function successCallback(response) {
        q.resolve(response);
      }, function errorCallback(response) {
        q.reject(response);
      });
      return q.promise;
    },
    getIntroText: function() {
      var q = $q.defer();
      $http({
        method: 'GET',
        timeout: 20000,
        withCredentials: false,
        url: "http://admin.perfectbbqapp.com/api/category/allScreens",
      }).then(function successCallback(response) {
        q.resolve(response);
      }, function errorCallback(response) {
        q.reject(response);
      });
      return q.promise;
    },
  };
});
