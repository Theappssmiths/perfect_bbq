var common = angular.module('starter.commonService', ['ionic']);

common.factory("LS", function($window) {
  return {
    setData: function(key, val) {
      $window.localStorage && $window.localStorage.setItem(key, val);
      return this;
    },
    getData: function(key) {
      return $window.localStorage && $window.localStorage.getItem(key);
    },
    removeData: function(key) {
      $window.localStorage && $window.localStorage.removeItem(key);
      return this;
    }
  };
});



common.factory("basicComponent", function($http, $ionicPopup, $ionicLoading, $cordovaNetwork) {
  var basic = {};

  basic.displayLoadingDiv = function() {
    $ionicLoading.show({
      template: '<ion-spinner></ion-spinner>'
    });
  },
  basic.hideLoadingDiv = function() {
    $ionicLoading.hide();
  },
  basic.setParams = function(obj){
    var str = "";
    for (var key in obj) {
        if (str != "") {
            str += "&";
        }
        str += key + "=" + encodeURIComponent(obj[key]);
    }
    return str;
  };
  return basic;
});


common.factory('pickImage', function($q, $ionicActionSheet, $cordovaCamera, basicComponent) {
  return {
    showActionsheet: function($scope) {
      var q = $q.defer();
      var hideSheet = $ionicActionSheet.show({
        buttons: [{
            text: 'Take Picture'
          },
          {
            text: 'From Gallery'
          },
        ],
        destructiveText: 'Cancel',
        cancel: function() {
          console.log('CANCELLED');
          basicComponent.hideLoadingDiv();
        },
        buttonClicked: function(index) {
          console.log('BUTTON CLICKED', index);
          if (index === 0) {
            var options = {
              quality: 80,
              destinationType: Camera.DestinationType.DATA_URL,
              sourceType: Camera.PictureSourceType.CAMERA,
              allowEdit: false,
              encodingType: Camera.EncodingType.JPEG,
              popoverOptions: CameraPopoverOptions,
              saveToPhotoAlbum: false,
              correctOrientation:true
            };
          } else {
            var options = {
              quality: 80,
              destinationType: Camera.DestinationType.DATA_URL,
              sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
              allowEdit: false,
              encodingType: Camera.EncodingType.JPEG,
              popoverOptions: CameraPopoverOptions,
              saveToPhotoAlbum: false
            };
          }

          $cordovaCamera.getPicture(options).then(function(imageData) {
            $scope.ImageURI = "data:image/jpeg;base64," + imageData;
            q.resolve($scope.ImageURI);
          }, function(error) {
            q.reject(error);
          });
          return true;
        },
        destructiveButtonClicked: function() {
          console.log("Button From Gallery");
          basicComponent.hideLoadingDiv();
          return true;
        }

      });
      return q.promise;
    }
  }
});
