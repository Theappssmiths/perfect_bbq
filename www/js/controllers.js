angular.module('starter.controllers', [])

  .controller('AppCtrl', function($scope, $rootScope, $state, $ionicHistory, $rootScope, ConnectivityMonitor, $window,
    $ionicScrollDelegate, getData, basicComponent, $timeout, ionicToast, $ionicSideMenuDelegate, $ionicLoading, recipeWS, LS,
    $ionicModal, $sce) {

    var searchCount;
    $rootScope.alignedTimer = [];
    $rootScope.language = 0;
    $rootScope.catAdv = [];
    $rootScope.recipeAdv = [];
    $scope.border = {
      visible: false
    };

    $scope.$on('$ionicView.enter', function(e) {

    });
    $scope.$on("$ionicView.beforeEnter", function() {
      console.log(window.screen, window, window.screen.availWidth);
      $scope.smWidth = window.screen.availWidth - 50;
      $scope.menuopen = false;
      $scope.sideMenuWidth = 290;
      $rootScope.about_title_image = '<img class="title_logo" src="img/about-logo.png">';
    });

    $scope.languages = {
      checked: true
    };
    $scope.search = {};
    $scope.searchList = [];


    //Watch menu is opened or not
    $scope.$watch(function() {
        return $ionicSideMenuDelegate.isOpen();
      },
      function(isOpen) {
        if (isOpen) {
          $scope.menuopen = true;
          //on cancel search
          $timeout(function() {
            $scope.cancelSearch = function() {
              if ($scope.menuopen) {
                $scope.noSearchItem = false;
                $scope.showCancel = false;
                $scope.search = {};
                $scope.sideMenuWidth = 290;
              }
            };
          }, 500);
        } else {
          $scope.menuopen = false;
          //on cancel search
          $scope.cancelSearch = function() {};
        }
      });

    //Go back to previous page
    $scope.back = function() {
      $ionicHistory.goBack();
    };

    //Watch scroll change of view
    $scope.getScroll = function() {
      var data = $ionicScrollDelegate.$getByHandle('about-content').getScrollPosition();
      if (data.top > 0) {
        $scope.border.visible = true;
      } else {
        $scope.border.visible = false;
      }
      $scope.$apply();
    }

    //handle menu search input
    $scope.searchItem = function(searchText) {
      $scope.noMoreSearch = true;
      if ($scope.search.text) {
        $ionicScrollDelegate.$getByHandle('menu-content').scrollTop();
        $scope.showCancel = true;
        $scope.sideMenuWidth = window.screen.width - 40;
        $scope.searchKeyword(searchText);
      } else {
        $scope.showCancel = false;
        $scope.search = {};
        $scope.sideMenuWidth = 290;
      }
    };

    $scope.searchInput = true;
    $scope.showSearch = function() {
      $scope.searchInput = false;
    }

    //Menu toggle
    $scope.rightOpen = function() {
      $ionicSideMenuDelegate.toggleRight();
      $ionicScrollDelegate.$getByHandle('menu-content').scrollTop();
      //cordova.plugins.Keyboard.close();
      if (!$scope.searchInput) {
        $scope.searchInput = true;
      }
    };

    //navigate to setting screen
    $scope.goToSetting = function() {
      if ($ionicSideMenuDelegate.isOpen()) {
        $ionicSideMenuDelegate.toggleRight();;
      }
      $state.go('app.setting');
    };

    $rootScope.hideSlide = true;
    //navigate to introduction screen
    $scope.goToIntro = function() {
      if ($ionicSideMenuDelegate.isOpen()) {
        $ionicSideMenuDelegate.toggleRight();;
      }
      //$state.go('intro');
      $scope.fromMenu = true;
      $rootScope.hideSlide = false;
      $scope.getIntroText();
    };

    //navigate to about screen
    $scope.goToAbout = function(id) {
      // if ($ionicSideMenuDelegate.isOpen()) {
      //   $ionicSideMenuDelegate.toggleRight();
      // }
      if (ConnectivityMonitor.isOnline()) {
        basicComponent.displayLoadingDiv();
        getData.getStaticDetail(id).then(function(result) {
          basicComponent.hideLoadingDiv();
          if (result.data.status) {
            $scope.staticData = result.data.data;
            if ($ionicSideMenuDelegate.isOpen()) {
              $ionicSideMenuDelegate.toggleRight();;
            }
            $state.go('app.about');
          } else {
            ionicToast.show(result.data.message, 'bottom', false, 2000);
          }
        }).catch(function(e) {
          basicComponent.hideLoadingDiv();
          ionicToast.show('Something went wrong. check network connection', 'bottom', false, 2000);
        });
      } else {
        ionicToast.show('Went offline. check network connection', 'bottom', false, 2000);
      }
    };


    $scope.trustedHtml = function(plainText) {
      return $sce.trustAsHtml(plainText);
    };


    //Select language
    $scope.selectedLanguage = function() {
      if ($scope.languages.checked == false) {
        LS.setData("bbqLanguage", 1);
        $rootScope.language = 1;
      } else {
        LS.setData("bbqLanguage", 0);
        $rootScope.language = 0;
      }
    }

    //change state
    $scope.stateChange = function(value) {
      console.log('valuse is:::', value);
      if (value != 'home' && value != 'roadmap') {
        console.log('else loadSubCategories');
        $scope.loadSubCategories(value.catId, value);
      } else {
        if ($ionicSideMenuDelegate.isOpen()) {
          setTimeout(function() {
            $ionicSideMenuDelegate.toggleRight();;
            if (value == 'home') {
              $state.go('app.home');
            } else if (value == 'roadmap') {
              $state.go('app.timerroadmap');
            }
          }, 500);
        }
      }
    };


    /* Advertisement Modal*/
    $ionicModal.fromTemplateUrl('templates/advertisement.html', {
      scope: $scope,
      animation: 'slide-in-up',
      cache: false,
      backdropClickToClose: false,
      hardwareBackButtonClose: false
    }).then(function(modal) {
      $scope.advModal = modal;
    });


    //Close advertisement
    $scope.closeAd = function() {
      $scope.advModal.hide();
      // if ($state.current.name === "app.element") {
      //   $rootScope.startTimer();
      // }
      // if ($state.current.name === "app.selectedElement") {
      //   $rootScope.startAdTimer();
      // }
    }

    //Show advertisement
    $scope.showAd = function() {
      $scope.advModal.show();
    }


    //Save coupon in gallery
    $scope.saveToGallery = function(image) {
      window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
        var fileTransfer = new FileTransfer();
        var uri = encodeURI(image);
        var path = fileSystem.root.toURL() + "PerfectBBQ/" + image.substring(image.lastIndexOf('/') + 1);
        fileTransfer.download(uri, path, function(entry) {
            window.cordova.plugins.imagesaver.saveImageToGallery(entry.nativeURL, onSaveImageSuccess, onSaveImageError);

            function onSaveImageSuccess() {
              refreshMedia.refresh(path);
              ionicToast.show('Coupon downloaded successfully, please check gallery.', 'bottom', false, 2000);
              $scope.closeAd();
              $scope.$apply();
            }

            function onSaveImageError(error) {
              ionicToast.show('SomeThing went wrong, please try again.', 'bottom', false, 2000);
            }

          },
          function(error) {
            ionicToast.show(error.code + '' + error.source, 'bottom', false, 2000);
          });
      });
    };



    //Search item in menu
    $scope.noSearchItem = false;
    $scope.searchKeyword = function(searchText) {
      searchCount = 1;
      if (ConnectivityMonitor.isOnline()) {
        $scope.noSearchItem = false;
        $scope.searchList = [];
        getData.searchRecipes(searchText, searchCount).then(function(result) {
          //basicComponent.hideLoadingDiv();
          if (result.data.status) {
            searchCount++;
            for (var i = 0; i < result.data.data.length; i++) {
              if (result.data.data[i].type == "category") {
                result.data.data[i].catImage = result.data.data[i].image;
                result.data.data[i].catId = result.data.data[i].id;
                result.data.data[i].catTitleEng = result.data.data[i].titleEng;
                result.data.data[i].catTitleDutch = result.data.data[i].titleDutch;
              } else {
                result.data.data[i].recId = result.data.data[i].id;
                result.data.data[i].recImage = result.data.data[i].image;
                result.data.data[i].recTitleEng = result.data.data[i].titleEng;
                result.data.data[i].recTitleDutch = result.data.data[i].titleDutch;
              }
              $scope.searchList.push(result.data.data[i]);
            }
            $scope.noMoreSearch = false;
          } else {
            $scope.noSearchItem = true;
          }
        }).catch(function(e) {
          ionicToast.show('Something went wrong. check network connection', 'bottom', false, 2000);
        });
      } else {
        ionicToast.show('Went offline. check network connection', 'bottom', false, 2000);
      }
    };


    //load more search item on scroll
    $scope.moreSearchKeyword = function(searchText) {
      if (ConnectivityMonitor.isOnline()) {
        getData.searchRecipes(searchText, searchCount).then(function(result) {
          if (result.data.status) {
            searchCount++;
            for (var i = 0; i < result.data.data.length; i++) {
              if (result.data.data[i].type == "category") {
                result.data.data[i].catImage = result.data.data[i].image;
                result.data.data[i].catId = result.data.data[i].id;
                result.data.data[i].catTitleEng = result.data.data[i].titleEng;
                result.data.data[i].catTitleDutch = result.data.data[i].titleDutch;
              } else {
                result.data.data[i].recId = result.data.data[i].id;
                result.data.data[i].recImage = result.data.data[i].image;
                result.data.data[i].recTitleEng = result.data.data[i].titleEng;
                result.data.data[i].recTitleDutch = result.data.data[i].titleDutch;
              }
              $scope.searchList.push(result.data.data[i]);
            }
            if ($scope.searchList.length >= result.data.total_records) {
              $scope.noMoreSearch = true;
            }
          } else {
            $scope.noMoreSearch = true;
          }
          $scope.$broadcast('scroll.infiniteScrollComplete');
        }).catch(function(e) {
          ionicToast.show('Something went wrong. check network connection', 'bottom', false, 2000);
        });
      } else {
        ionicToast.show('Went offline. check network connection', 'bottom', false, 2000);
      }
    };

    $scope.invite = function(index) {
      if ($ionicSideMenuDelegate.isOpen()) {
        $ionicSideMenuDelegate.toggleRight();
      }
      $state.go("app.invite");
    }


    //Search funrecipe if no subcategory exist
    $scope.searchRecipe = function(item) {
      if (ConnectivityMonitor.isOnline()) {
        basicComponent.displayLoadingDiv();
        $scope.recipeList = [];
        $scope.selectedSubRecipe = item;
        recipeWS.getRecipes(item.catId, 1).then(function(result) {
          basicComponent.hideLoadingDiv();
          if (result.data.status) {

            if (result.data.advMode === "1") {
              $rootScope.recipeAdv = result.data.advData;
            } else {
              $rootScope.recipeAdv = [];
            }

            for (var i = 0; i < result.data.data.length; i++) {
              result.data.data[i].isSelected = false;
              $scope.recipeList.push(result.data.data[i]);
            }
            if ($ionicSideMenuDelegate.isOpen()) {
              $ionicSideMenuDelegate.toggleRight();
            }
            $state.go('app.selectedElement', {
              stateParam: item
            });
          } else {
            ionicToast.show(result.data.message, 'bottom', false, 2000);
          }
        }).catch(function(e) {
          basicComponent.hideLoadingDiv();
          ionicToast.show('Something went wrong. check network connection', 'bottom', false, 2000);
        });
      } else {
        ionicToast.show('Went offline. check network connection', 'bottom', false, 2000);
      }
    };

    //load subcategory
    $scope.loadSubCategories = function(id, item) {
      if (item.key) {
        if (item.key === 'invite') {
          $scope.invite();
        } else {
          $scope.loadBBQBasics();
        }
      } else {
        if (ConnectivityMonitor.isOnline()) {
          basicComponent.displayLoadingDiv();
          getData.getAllSubCategories(id, 1).then(function(result) {
            $scope.subCategories = [];
            $scope.selectedSubCategory = item;
            if (result.data.status) {
              basicComponent.hideLoadingDiv();

              if (result.data.advMode === "1") {
                $rootScope.catAdv = result.data.advData;
              } else {
                $rootScope.catAdv = [];
              }

              $scope.subCategories = result.data.data;
              if ($ionicSideMenuDelegate.isOpen()) {
                $ionicSideMenuDelegate.toggleRight();

              }
              $state.go('app.element', {
                stateParam: item
              });
            } else {
              if (result.data.total_records == 0) {
                $scope.searchRecipe(item);
              }
            }
          }).catch(function(e) {
            basicComponent.hideLoadingDiv();
            ionicToast.show('Something went wrong. check network connection', 'bottom', false, 2000);
          });
        } else {
          ionicToast.show('Went offline. check network connection', 'bottom', false, 2000);
        }
      }
    };

    //load BBQ Basic
    $scope.loadBBQBasics = function() {
      if (ConnectivityMonitor.isOnline()) {
        basicComponent.displayLoadingDiv();
        getData.getBBQBasics(1).then(function(result) {
          $scope.bbqBasics = [];
          if (result.data.status) {
            basicComponent.hideLoadingDiv();
            $scope.bbqBasics = result.data.data;
            if ($ionicSideMenuDelegate.isOpen()) {
              $ionicSideMenuDelegate.toggleRight();
            }
            $state.go('app.bbqbasiclist');
          }
        }).catch(function(e) {
          basicComponent.hideLoadingDiv();
          ionicToast.show('Something went wrong. check network connection', 'bottom', false, 2000);
        });
      } else {
        ionicToast.show('Went offline. check network connection', 'bottom', false, 2000);
      }
    };

    //Load selected recipe detail
    $scope.getRecipeDetail = function(item) {
      item.isSelected = true;
      if (ConnectivityMonitor.isOnline()) {
        basicComponent.displayLoadingDiv();
        recipeWS.getRecipeDetail(item.recId).then(function(result) {
          basicComponent.hideLoadingDiv();
          if (result.data.status) {
            $rootScope.recDetail = result.data.data;
            if ($ionicSideMenuDelegate.isOpen()) {
              $ionicSideMenuDelegate.toggleRight();
            }
            $state.go('app.preparationScreen', {
              stateParam: item
            });
            item.isSelected = false;
          }
        }).catch(function(e) {
          basicComponent.hideLoadingDiv();
          ionicToast.show('Something went wrong. check network connection', 'bottom', false, 2000);
        });
      } else {
        ionicToast.show('Went offline. check network connection', 'bottom', false, 2000);
      }
    };

    //load category
    $rootScope.getCategories = function() {
      if (ConnectivityMonitor.isOnline()) {
        basicComponent.displayLoadingDiv();
        $scope.categories = [];
        $scope.fixed_categories = [];
        getData.getAllCategories(1).then(function(result) {
          basicComponent.hideLoadingDiv();
          if (result.data.status) {
            $scope.categories = result.data.data;
            $scope.fixed_categories = result.data.page_data;
            $scope.pushInviteBasic();
          } else {}
        }).catch(function(e) {
          basicComponent.hideLoadingDiv();
          ionicToast.show('Something went wrong. check network connection', 'bottom', false, 2000);
        });
      } else {
        ionicToast.show('Went offline. check network connection', 'bottom', false, 2000);
      }
    };


    $scope.pushInviteBasic = function() {
      $scope.categories.push({
        catAdvSwitch: "0",
        catId: "23",
        catImage: "http://admin.perfectbbqapp.com/bbq_images/15055818305-invite-225x148.png",
        catLayout: "Grid",
        catParentId: "0",
        catParentNameDutch: "",
        catParentNameEng: "",
        catTitleDutch: "Freunde einladen",
        catTitleEng: "Invite friends",
        key: 'invite'
      })

      $scope.categories.push({
        catAdvSwitch: "0",
        catId: "24",
        catImage: "http://admin.perfectbbqapp.com/bbq_images/15055818736-basics-225x148.png",
        catLayout: "List",
        catParentId: "0",
        catParentNameDutch: "",
        catParentNameEng: "",
        catTitleDutch: "Perfect BBQ Grundlagen",
        catTitleEng: "Perfect BBQ Basics",
        key: 'basics'
      })
    };

    //navigation based on timer condition
    $scope.openCategory = function() {
      if ($rootScope.alignedTimer.length == 4) {
        ionicToast.show('You have already added maximum 4 timers.', 'bottom', false, 2000);
      } else {
        $ionicHistory.clearCache();
        $ionicHistory.clearHistory();
        $state.go('app.home');
        $rootScope.getCategories();
      }
    };
    var oldd;
    //watch timer notification event
    document.addEventListener("resume", function() {

      console.log("event_calling_resume");
      console.log('resume', $rootScope.alignedTimer);
      var d = new Date(); // for now
      d.getHours(); // => 9
      d.getMinutes(); // =>  30
      d.getSeconds(); // => 51
      console.log('time when resume::'+ d.getHours() +':'+  d.getMinutes() +':'+d.getSeconds());
      
      console.log('Old::',oldd.getTime(), "New::", d.getTime());
      console.log('difference:::', d.getTime() - oldd.getTime());
      var dif = d.getTime() - oldd.getTime();
      console.log('sec dif:::', dif/1000);
      console.log(Math.floor(dif/1000));
      var ip = Math.floor(dif/1000);
      console.log('ip::', ip);
      console.log('$rootScope.timeTick resume', $rootScope.timeTick, $rootScope.timeTick.length);

      for (var i = 0; i< $rootScope.alignedTimer.length; i++) {
        console.log('Hello:::', $rootScope.timeTick[i+1]);
      }

      //cordova.plugins.backgroundMode.disable();
      $rootScope.onTimerScreen = true;
    }, false);

    document.addEventListener("pause", function() {
      // if ($rootScope.alignedTimer.length > 0) {
      //   console.log("event_calling_pause");
      // }
      console.log("event_calling_pause");
      console.log('pause', $rootScope.alignedTimer);
      oldd = new Date();
      var d = new Date(); // for now
      d.getHours(); // => 9
      d.getMinutes(); // =>  30
      d.getSeconds(); // => 51
      console.log('time when pause::'+ d.getHours() +':'+  d.getMinutes() +':'+d.getSeconds());
      console.log('$rootScope.timeTick pause', $rootScope.timeTick, $rootScope.timeTick.length);


      $rootScope.onTimerScreen = false;
    }, false);




    //load intoText
    $scope.getIntroText = function() {
      if (ConnectivityMonitor.isOnline()) {
        if ($scope.fromMenu) {
          basicComponent.displayLoadingDiv();
        }
        $scope.categories = [];
        getData.getIntroText().then(function(result) {
          basicComponent.hideLoadingDiv();
          if (result.data.status) {
            $scope.introData = result.data.data;
            $state.go('intro');
            $scope.fromMenu = false;
          }
        }).catch(function(e) {
          basicComponent.hideLoadingDiv();
          ionicToast.show('Something went wrong. check network connection', 'bottom', false, 2000);
        });
      } else {
        ionicToast.show('Went offline. check network connection', 'bottom', false, 2000);
      }
    };

    //http://admin.perfectbbqapp.com/
    //$window.localStorage.removeItem('bbqLanguage');
    document.addEventListener('deviceready', function() {
      cordova.plugins.notification.local.registerPermission(function(granted) {
        console.log('Permission has been granted: ' + granted);
      });
      if (LS.getData('bbqLanguage')) {
        $rootScope.language = LS.getData('bbqLanguage');
        if ($rootScope.language == 1) {
          $scope.languages.checked = false;
        } else {
          $scope.languages.checked = true;
        }
        $scope.openCategory();
      } else {
        //$state.go('intro');
        $scope.getIntroText();
      }
    });

  });
